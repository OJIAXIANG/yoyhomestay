<!DOCTYPE html>
<html>
<title>YOY HOMESTAY</title>
<head>
<link rel="stylesheet" type="text/css" href="image.css">
<link rel="stylesheet" type="text/css" href="login.css">
<link rel="stylesheet" type="text/css" href="scrollbar.css">
</head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "FZCuQian-M17S", Arial, Helvetica, sans-serif}
.w3-display-center 
{
  font-family:"Arial";
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  font-size:74px;
  color: white;
  border: 0px solid #f1f1f1;
  top: 50%;
  left: 50%;
  position : absolute;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 100%;
  padding: 20px;
  text-align:left;
}
</style>
<body>

<!-- Navigation Bar -->
<div id="homestay">
<?php include("header.php")?>
</div>
<!-- Header -->
<header class="w3-display-container w3-content" style="max-width:85%;">
  <img class="w3-image"style="filter: blur(8px);-webkit-filter: blur(2px);position:relative;"
  src="https://1.bp.blogspot.com/-HlfJ7JkdAfI/X0ff5fjBwzI/AAAAAAAAFRQ/M_4yn-YLpOY_WHvYCVizWq4ayPKgLOrXQCLcBGAsYHQ/s1600/house-beautiful.jpg" alt="The Hotel" style="min-width:1000px" width="100%">
  <div class="w3-display-center w3-padding w3-col 20 m8">
    <div class="w3-container">
      <p>Finding, Booking, <br>Searching The Best Homestay.</p>  
    </div>
  </div> 
</header>
<!-- Page content -->
<div class="w3-content"  style="max-width:1532px; margin-top:80px;">
  <div class="w3-row-padding" id="about">
    <div class="w3-col l4 12">
      <h3>About</h3>
      <p>YoY Homestay is Melaka's finest homestay booking website for comfy, cozy places to 
       stay, offering more than 10 accomodations all powered by local hosts. </p>
      <p>This website is led by 3 Multimedia University Students, Ong Jia Xiang, Yeow Yi Feng
       and Yap Tung Quan who formed the homestay booking website in 2020, with the aim of :-
      <br>
  <ul>
	<li>Letting the tourists have convenience of finding homestays.</li>
	<li>Have a proper local experience.</li>
	<li>Increase the rate of homestay booking.</li>
  </ul>
    </div>
    <div class="w3-col l8 12">
      <!-- Image of location/map -->
      <img src="https://1.bp.blogspot.com/-n9KujDtHs04/Xv18R2fJXtI/AAAAAAAAEWM/8FaiNOOUMZAX3pAr7v5QZRpPjUi24BBLgCLcBGAsYHQ/s1600/topview%25282%2529.jpg" class="w3-image " style="width:100%;">
    </div>
  </div> 
  <div class="w3-row-padding w3-large w3-center" style="margin:32px 0">
    <div class="w3-third"><i class="fa fa-map-marker w3-text-red"></i> Location: Bukit Beruang,Melaka</div>
    <div class="w3-third"><i class="fa fa-phone w3-text-red"></i> Phone: 011-36692463</div>
    <div class="w3-third"><i class="fa fa-envelope w3-text-red"></i> Email: yoyhomestay.gmail.com</div>
  </div>

 <br>
  <div class="w3-container">
    <h3>Our Homestay</h3>
    <h6>You can find our homestay anywhere in the Malacca:</h6>
  </div>
  
  <div class="w3-row-padding w3-padding-16 w3-text-white w3-large">
    <div class="w3-half w3-margin-bottom">
      <div class="w3-display-container">
        <img src="https://1.bp.blogspot.com/-ngLV0f4o2_E/XvzMrm5bc1I/AAAAAAAAEUc/Gu6HpnZnl6o-XhlsoM3atKF3xKXnLxFjQCLcBGAsYHQ/s1600/bandarhilir%25287%2529.jpg" alt="bandar hilir" style="width:100%" >
	
        <span class="w3-display-bottomleft w3-padding">Bandar Hilir</span>
    </div>
    </div>
    <div class="w3-half">
      <div class="w3-row-padding" style="margin:0 -16px">
        <div class="w3-half w3-margin-bottom">
          <div class="w3-display-container">
            <img src="https://1.bp.blogspot.com/-OKPq4L9s1Qs/Xvy7l2BJiwI/AAAAAAAAESc/JDPmsQ3Z6o0yYysjt5BOvIbkJ_M1orR_wCLcBGAsYHQ/s1600/alorgajah.jpg" alt="Alor Gajah" style="width:100%">
            <span class="w3-display-bottomleft w3-padding">Alor Gajah</span>
          </div>
        </div>
        <div class="w3-half w3-margin-bottom">
          <div class="w3-display-container">
            <img src="https://1.bp.blogspot.com/-8Tz-XpZ0l-c/Xvy7lyR5xAI/AAAAAAAAESU/D439Niyre84m2whXqhn7osrQmjaAJYyigCLcBGAsYHQ/s1600/jasin3.jpg" alt="Jasin" 
			style="width:100%">
            <span class="w3-display-bottomleft w3-padding">Jasin</span>
          </div>
        </div>
      </div>
      <div class="w3-row-padding" style="margin:0 -16px">
        <div class="w3-half w3-margin-bottom">
          <div class="w3-display-container">
            <img src="https://1.bp.blogspot.com/-JI5FUHDaEoA/Xv16KTY5ViI/AAAAAAAAEV4/E9z9Bwzl2es57Oh81HMkUTibRxL3D9WPQCLcBGAsYHQ/s1600/tanjunkling%25284%2529.jpg" alt="Tanjung Kling" style="width:100%">
            <span class="w3-display-bottomleft w3-padding">Tanjung Kling</span>
          </div>
        </div>
        <div class="w3-half w3-margin-bottom">
          <div class="w3-display-container">
            <img src="https://1.bp.blogspot.com/-yNmUTZT9cBE/Xv11-lP9nVI/AAAAAAAAEVQ/9QFQDwDg0ewQ24vL_pO3KdMwFWtxpdRhwCLcBGAsYHQ/s1600/melakariver%25281%2529.jpg" alt="Melaka River" style="width:100% ">
            <span class="w3-display-bottomleft w3-padding">Melaka River</span>
          </div>
        </div>
      </div>
    </div>
  </div>

	<br>

<!-- End page content -->
</div>

<!-- Footer -->
<?php include("footer.php");?>

</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

  $(function(){

    // create new string
    var dateToday = new Date();
    
    // get month, day, year 
    var month = dateToday.getMonth() + 1;
    var day = dateToday.getDate();
    var minYear = dateToday.getFullYear();
    var maxYear = dateToday.getFullYear() + 1;

    // set month = 07 if month = 7
    if(month < 10)
        month = '0' + month.toString();

    // set day = 08 if day = 8
    if(day < 10)
        day = '0' + day.toString();  
    
    // set the year, month, day to complete string
    var maxDate = maxYear + '-' + month + '-' + day; 
    var minDate = minYear + '-' + month + '-' + day;
    // set the custom minDate to your input CheckIn
    $('#CheckIn').attr('min',minDate);
    
    // set custom maxDate to your input CheckOut
    $('#CheckOut').attr('max',maxDate);

    // set the function to checkout validation checkin date
    var checkinDate = $("#CheckIn").val();	
    $("#CheckIn").change(()=>{
    if(checkinDate != null || checkinDate != "")
    {
      var checkoutDate = $('#CheckIn').val();
      $('#CheckOut').attr('min',checkoutDate); 
    }
   
    });
});
</script>
<?php
    if (isset($_POST['useremail'],$_POST['userpsw']))
           {
            $email=$_POST['useremail'];
            $password=$_POST['userpsw'];

               if (empty($email) || empty($password))
               {
                  $error = 'Hey All fields are required!!';
                }
                 
                 else {  
                 $login="select * from signup_info where Guest_Name='".$email."' and Guest_password ='".$password."'";
                 $result=mysqli_query($con,$login);
                if(mysqli_fetch_array($result,MYSQLI_NUM)){
             $_SESSION['logged_in']='true';
             $_SESSION['useremail']=$email;
                 header('Location:registration.php');
                 exit();
                 } else {
                 $error='Incorrect details !!';
                 }
                       }
    }

?>