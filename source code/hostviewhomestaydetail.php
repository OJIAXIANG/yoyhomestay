<!DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="scrollbar.css">
<style>
#menu-image img
{
 mx-auto p-auto mt-3 d-flex justify-content-center
}
</style>
</head>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">


<body>
<?php include("header.php"); ?>
  <div class="container border shadow mt-3 w-50 d-flex justify-content-center">
    <h1>Detail of the Homestay</h1>
  </div>
<?php
  	include("dataconnection.php");
	
	
	if(isset($_GET["id"]))
	{	//add
		$approved_id=$_GET["id"];
		
	}else
	{
		$approved_id="";
	}	
	if($connect->connect_error)
	{
		die("Connection failed:".$conn->connect_error);
	}
	
	$host_id = $_SESSION['hostid'];

	$sql = "SELECT * from approved_homestay where approved_id = '$approved_id'";
	
	$result = $connect->query($sql);
	//take result to display
	$row=mysqli_fetch_assoc($result);
	
	
		
?>	


	<div class="container  mt-3 mb-5 d-flex justify-content-center w-85 shadow-lg">
		<div class="container">
		<form action="#" method="POST">
		
		
			<div class="row mt-5 align-items-center"> 
				<div class="col">  
					<?php
						$dir  ='imagepreview1/';
						// Image selection and display:
						
						echo "<img style='width:300px; height:250px;' src='$dir".$row['image']."' >";
					?>
					
				</div>
					<div class="col " style="font-size:20px">
						<?php echo $row["image_text"]; ?>
					</div>
				<div class="col">	
					<?php
						$dir  ='imagepreview1/';
						
						echo "<img style='width:300px; height:250px;' src='$dir".$row['image2']."' >";
					?>
					
				</div>
				
				
		
		<div class="col" style="font-size:20px">
				<?php echo $row["image_text2"];?>
			</div>
		</div>
	
			
		
		<div class="row mt-5">  
            <div class="col" style="font-size:30px"> 
				<p>Homestay Title:  <?php echo $row["homestay_name"];?></p>
			</div>
        </div> 
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px"> 
				<p>
					<p>About the homestay:  <?php echo $row["about_homestay"];?><br></input></p>
				</p>
			</div>
        </div>
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px">
				<p>
					<p>Address Line:  <?php echo $row["address_line"];?><br></input>
				</p>
		    </div>
        </div> 
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px">		
				<p>
					<p>Full Address:  <?php echo $row["full_address"];?></p>
				</p>
			</div>
        </div>

		<div class="row mt-1">  
            <div class="col" style="font-size:20px">	
				<p>
					<p>House Type:   <?php echo $row["house_type"];?></p>
				</p>
			</div>
        </div>

		<div class="row mt-1">  
            <div class="col" style="font-size:20px">
				<p>
					<p>Number of room in a homestay: <?php echo $row["number_of_room"];?></p>
				</p>
			</div>
        </div>	
		<div class="row mt-1">  
            <div class="col" style="font-size:20px">
				<p>
					<p>Price per night : <?php echo $row["price_per_night"];?> </p>
				</p>
			</div>
        </div>
	</form>
		
	</div>
	
   <?php	
		
?>  
</div>