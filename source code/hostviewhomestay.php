<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>  
<script>
//create a javascript function named confirmation()
	function confirmation()
	{
		var answer;
		answer=confirm("Do you want to delete this homestay?");
		return answer;
	}
	</script>
<body>
<?php include("header.php"); 
    include("dataconnection.php");?> 
<div class="container border shadow mt-5 w-25 d-flex justify-content-center bg-white rounded">
    <h2><i class="fa fa-home w3-xxlarge w3-text-teal">&nbsp;</i>HOMESTAY LIST</h2>
</div>
<div class="container border shadow mt-5 w-100 d-flex justify-content-center bg-white rounded">
<table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Homestay Name</th>
        <th scope="col">Address-Line</th>
        <th scope="col">House-type</th>
        <th scope="col"></th>
        </tr>
    </thead>
<?php 
    $id = $_SESSION['hostid'];
    $sql = "SELECT * from upload_homestay where Host_ID='$id'";
    $result = mysqli_query($connect,$sql);
    if(!$result) {
        die('Error: ' . mysqli_error($connect));
    }
    else if($result)
    {   $num = 1;
        while($row = mysqli_fetch_assoc($result))
        {        
        $homestay = $row['homestay_name'];
        $add = $row['address_line'];
        $ht = $row['house_type'];
		$date = date("m/d/Y",strtotime('+3 day'));
		$dis = "Please upload your homestay.";
        ?>
    <tbody>
        <tr>
        <td><?php echo $num; ?></td>
        <td><?php echo $homestay;?></td>
        <td><?php echo $add; ?></td>
        <td><?php echo $ht; ?></td>
		<td> 
			<form action="cancelbooking.php">
			 <?php echo "<button class='btn btn-outline-info btn-sm' disabled>Pending...</button>"; ?>
			</form>	
        </td>
        </tr>
        <?php 
         $num++;  
                }
		}
        ?>  
        <tr>
            <td colspan="7">
        <p style="color:red;">***Your homestay detail has been processing. Please wait for the approval...</p>  
            </td>
         </tr>
    </tbody>    
    </table>
</div>
<div class="container border shadow mt-5 w-100 d-flex justify-content-center bg-white rounded">
<table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Homestay Name</th>
        <th scope="col">Address-Line</th>
        <th scope="col">House-type</th>
        <th scope="col"></th>
        </tr>
    </thead>
<?php 
    $id = $_SESSION['hostid'];
    $sql = "SELECT * from approved_homestay where Host_ID='$id'";
    $result = mysqli_query($connect,$sql);
    if(!$result) {
        die('Error: ' . mysqli_error($connect));
    }
    else
    {   $num = 1;
        while($row = mysqli_fetch_assoc($result))
        {        
		$idhome = $row['approved_id']; 
        $homestay = $row['homestay_name'];
        $add = $row['address_line'];
        $ht = $row['house_type'];
        $date = date("m/d/Y",strtotime('+3 day'));
        ?>
    <tbody>
        <tr>
        <td><?php echo $num; ?></td>
        <td><?php echo $homestay;?></td>
        <td><?php echo $add; ?></td>
        <td><?php echo $ht; ?></td>
        <td> 
			 <a class='btn btn-outline-info btn-sm' href="hostviewhomestaydetail.php?id=<?php echo $idhome; ?>">Details</a>
		</td>
		<td> 
			<form action="" method="GET"> 
			<a class='btn btn-outline-info btn-sm' onclick="return confirmation()" href="deletehomestay.php?home=<?php echo $idhome; ?>">Delete</a>
			</form>
		</td>
        </tr>
        <?php 
         $num++;  
                }
            }
        ?>  
        <tr>
            <td colspan="7">
        <p style="color:red;">***If you want to delete your homestay, please make sure there is no booking from the guests.</p>  
            </td>
         </tr>
    </tbody>    
    </table>
</div>
</body>  
