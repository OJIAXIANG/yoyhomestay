<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>  
<body>
<?php include("header.php"); 
    include("dataconnection.php");?> 
<div class="container border shadow mt-5 w-25 d-flex justify-content-center bg-white rounded">
    <h2><i class="fa fa-money w3-xxlarge w3-text-teal">&nbsp;</i>BOOK LIST</h2>
</div>
<div class="container border shadow mt-5 w-100 d-flex justify-content-center bg-white rounded">
<table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Homestay Name</th>
        <th scope="col">Checkin-Date</th>
        <th scope="col">Checkout-Date</th>
        <th scope="col">Total-Price</th>
        <th scope="col">Number Of Nights</th>
        <th scope="col"></th>
        </tr>
    </thead>
<?php 
    $id = $_SESSION['id'];
    $sql = "SELECT * from booking where Guest_ID='$id'";
    $result = mysqli_query($connect,$sql);
    $sqla = "SELECT * from pay where Guest_ID='$id'";
    $resulta = mysqli_query($connect,$sqla);
    if(!$result) {
        die('Error: ' . mysqli_error($connect));
    }
    else
    {  
        if(!$resulta) {
            die('Error: ' . mysqli_error($connect));
        }
        else
        { $num = 1;
        while($row = mysqli_fetch_assoc($result))
        {        
        $rowa = mysqli_fetch_assoc($resulta);

        $homestay = $row['homestay_name'];
        $in = $row['checkin'];
        $out = $row['checkout'];
        $days = $row['num_days'];
        $total = $rowa['totalprice'];
        $date = date("m/d/Y",strtotime('+3 day'));
        $today = date("m/d/Y");
        ?>
    <tbody>
        <tr>
        <td><?php echo $num; ?></td>
        <td><?php echo $homestay;?></td>
        <td><?php echo $in; ?></td>
        <td><?php echo $out; ?></td>
        <td>RM : <?php echo $total;?></td>
        <td><?php echo $days;?> nights</td>
        <td> 
            <form action="" method="GET">
            <?php if($out<=$today){  
                ?>
               <a href="feedback.php?home=<?php echo $homestay?>" class='btn btn-outline-info btn-sm'>MORE</a><?php
            }
            else{
             //echo "<button class='btn btn-outline-info btn-sm' disabled>PASSED</button>";
            }?>
            </form>
        </td>
        <td> 
            <form action="" method="GET">
            <?php if($in>=$date){  
                ?>
               <a href="cancelbooking.php?cancel=<?php echo $row["book_id"]?>" class='btn btn-outline-info btn-sm'>CANCEL</a><?php
            }
            else{
             echo "<button class='btn btn-outline-info btn-sm' disabled>CONFIRM</button>";
            }?>
            </form>
        </td>
        </tr>
        <?php 
         $num++;  
                    }
                }
            }
        ?>  
        <tr>
            <td colspan="7">
        <p style="color:red;">***The customer is unable to cancel the booking 3 days before the booking date.</p>  
        <p style="color:red;">***You may write the feedback/rating by clicking the [MORE] button after you have checked out from the homestay.</p>  
            </td>
         </tr>
    </tbody>    
    </table>
</div>
</body>  
