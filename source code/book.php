<!DOCTYPE html>
<html>
<head></head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<!-- Navigation Bar -->
<?php include("header.php");?>
<?php 
 if(isset($_GET['home']))
 {
   $homeid = $_GET['home'];
 }
 else
 {
   $homeid = "";
 }
 if(isset($_GET['checkin'])&& isset($_GET["checkout"])){
   $checkin = $_GET['checkin'];
   $checkout = $_GET['checkout'];
 }
 else
 {
   $checkin = $checkout = "";
 }
?>
<?php 
    include("dataconnection.php");
    if($connect->connect_error)
    {
      die("Connection failed:".$conn->connect_error);
    }
    if(isset($_GET['w1']) && isset($_GET["w4"]))
    {
      $days=$_GET['w1'];
      $guest=$_GET['w4'];
    }
    else{
      $days=$guest="";
    }
    $book = "";
    $sql = "SELECT * from approved_homestay where approved_id = '$homeid'";
    $result = $connect->query($sql);
    if(mysqli_num_rows($result)==1)
    {
      $row = mysqli_fetch_assoc($result);
?>  
<body>
  <div class="container border shadow mt-5 w-25 d-flex justify-content-center">
    <h3><i class="fa fa-home w3-xxlarge"></i>Booking HomeStay</h3>
  </div>
  <input type="text" id="homeid" value="<?php echo $homeid ?>" style="display:none;" >
	<!-- homestay name,image,details -->
    <div class="container  mt-5  d-flex justify-content-center w-50 shadow-lg rounded">
      <div class="container">
      <form action="#" method="POST"> 
        <div class="row">  
          <div class="col">        
            <a style="font-size:25px">Homestay Name :    </a>           
                <?php
                echo " <a style='font-size:25px;'> ".$row['homestay_name']."</a>"
                ?>
          </div>
        </div>
        <div class="row mt-3">  
          <div class="col"> 
          About Homestay :
                <?php
                echo " <a style='font-size:15px;'> ".$row['about_homestay']."</a>"
                ?>
           </div>
        </div>   
        <div class="row mt-3">  
          <div class="col">        
            House type :               
                <?php
                echo " <a style='font-size:15px;'> ".$row['house_type']."</a>"
                ?>
          </div>
        </div>
        <div class="row mt-3">  
          <div class="col">        
             Homestay Location :               
                <?php
                echo " <a style='font-size:15px;'> ".$row['full_address']."</a>"
                ?>
          </div>
        </div>
        <div class="row mt-3">  
          <div class="col"> 
            Price Per Night  :   RM      
                <?php              
                echo " <a style='font-size:15px;'> ".$row['price_per_night']."</a>"
                ?>              
          </div>  
        </div>     
        <div class="row mt-3 ">  
          <div class="col"> 
            House picture  :         
                <?php
                  $dir  ='imagepreview1/';
                  echo "<img src='$dir".$row['image']."' style='width:200px; height:220px;'>";?>
          </div>
          <div class="col">         
                <?php  
                  echo "<img src='$dir".$row['image2']."' style='width:200px; height:220px;'><br>";
                ?>
          </div>  
        </div>         
      
            <div class="row mt-3">
              <div class='col'>
                <div class="form-group">
                <label for="checkin">Checkin</label>
                  <input type="text" name="checkin" class="form-control clickable input-md" value="<?php echo $checkin ?>" 
                  id="checkin" onchange="hello()"  placeholder=" Check-In" required>
                </div>
              </div>         
              <div class='col'>
                <div class="form-group">
                <label for="checkout">Checkout</label>
                    <input type="text" name="checkout" class="form-control clickable input-md" value="<?php echo $checkout ?>" 
                    id="checkout" onchange="getdays()" placeholder="Check-Out"required>
                </div>
              </div>
            </div>
            <div class="row">
              <div class='col'>
                <div class="form-group">
                <label for="guest">Number Of Guest</label>
                    <input type="number" class="form-control clickable input-md" name="guest" value="<?php echo $guest ?>"oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" id="guest" min="1" max="15" required>                          
                </div>
              </div>    
              <div class='col'>
                <div class="form-group">
                <label for="days">Number Of days</label>
                    <input type="number" class="form-control clickable input-md" name="days" value="<?php echo $days ?>"id="days" readonly required onclick="getdays();">                          
                </div>
              </div>                        
            </div>   
          <hr>     
          <!-- ///date picker -->
            
    <button class="btn btn-info shadow mb-4"  type="submit" name="submit"> Pay & Book</button>
      </div> 
    </div>
<?php
    }
?>

</body>
</html>
<?php
if(isset($_POST['submit'])){
  $hostid = $row['Host_ID'];
  $approved_id = $row['approved_id'];
  $price = $row['price_per_night'];
  $checkin = $_POST['checkin'];
  $checkout = $_POST['checkout'];
  $guest = $_POST['guest'];
  $days = $_POST['days'];
  $id = $_SESSION['id'];
  $_SESSION['home'] = $row['homestay_name'];
  $home = $_SESSION['home'];
  ?>
  <?php
  $sql = "INSERT into booking(homestay_name,book_price,checkin,checkout,num_guest,num_days,approved_id,Guest_ID,active,Host_ID)
  value('$home','$price','$checkin','$checkout','$guest','$days','$approved_id','$id','1','$hostid') ";
  if(!mysqli_query($connect,$sql)){
    echo "<script>alert('sorry to say book is not successfully!');</script>";
    echo "<script>window.location.href = 'homestaybrowse.php';</script>";
  }
  else{?>
   <script>      
            alert('Please make a payment and thank you!');        
            window.location.href = "payment.php?bookid=1" ;
    </script>
    <?php
  }
}
?>
<script>
  // function hello(){
  //   var homeid = $("#homeid").val();
  //   var checkoutDate = $('#checkout').val();
  //   var checkinDate = $('#checkin').val();
  //   console.log("hi");	
  //   console.log(homeid);	
  //   if(homeid!="" || homeid!="Null" )
  // {
	// self.location= "book.php?checkin="+checkinDate +"&checkout="+checkoutDate +"&home="+homeid;
  // }}

  function getdays(){
  var checkinDate = $('#checkin').val();	
  var checkoutDate = $('#checkout').val();
  var total = Date.parse(checkoutDate) - Date.parse(checkinDate);
  var days = Math.floor( total/(1000*60*60*24) );
  var guest = $("#guest").val();
  var homeid = $("#homeid").val();
  if(days!="" || days!="Null" )
  {
	self.location= "book.php?w1="+days + "&w4="+guest+ "&checkin="+checkinDate + "&checkout="+checkoutDate+"&home="+homeid;
  }}
</script>
<?php
  $sql = "SELECT * from booking where approved_id = '$homeid'";
  $result = mysqli_query($connect,$sql);
  if(mysqli_num_rows($result)>0) 
  {
    while($row = mysqli_fetch_array($result))
    {   
        $getdate[] = array(
          'checkin' => $row['checkin'],
          'checkout' => $row['checkout']
        );     
    }
    //echo count($getdate);
    //echo json_encode($getdate);
    for($x=0;$x<count($getdate);$x++)
    {
      $in = date_create($getdate[$x]['checkin']);
      // echo date_format($in,"m/d/Y");?><br><?php
      $out = date_create($getdate[$x]['checkout']);
      // echo date_format($out,"m/d/Y");?><br><?php
      $out = $out->modify( '+1 day' );
      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($in, $interval ,$out);
      foreach($daterange as $date){
        $dates[] = $date->format("m/d/Y");
        //array_push($dates,$date->format("m/d/y"));
          //return $dates;
          }
    }
      $disabledates =  json_encode($dates);
      //echo $disabledates;
  }
  else{
    $disabledates = '["01\/01\/2020"]';
  }
?>
<?php
// if(isset($_GET['checkin'])){
//   $checkin=$_GET['checkin'];
//   $sql = "SELECT * from booking where approved_id = '$homeid'";
//   $result = mysqli_query($connect,$sql);
//   if(!mysqli_num_rows($result)==1){
//     die('Error: ' . mysqli_error($connect));
//   }
//   else{ 
//     while($row = mysqli_fetch_array($result))
//     {    
//          $getdate[] = array(
//         'checkin' => $row['checkin']
//       );     
//     }
//     for($x=0;$x<count($getdate);$x++)
//     {
//         // 12 cin 1 data
//         // 18 cin 2 data then loop 16 less than 18 so my max date is 18 
//         // 14 cin because looping wow
//         // 16 checkin is mean the $checkin direct to +3m no go loop should if (checkin loop) else checkin=GETchekcin then +3m maybe?
//         // 16 to 18 jie yu asking ma
//         $in = date_create($getdate[$x]['checkin']);
//         $datacin = date_format($in,"m/d/Y"); 
//         if($checkin<=$datacin)
//         {
//             $checkin = $datacin;
//         }
//         else
//         {
//           $checkin = $checkin;
//         }
//     }
//   }
// }
// else{
//   $checkin="+6m";
// } echo $checkin;
?>
<script>
$( function() {   
  var dates = <?php echo $disabledates ?>;
  //var dates = ["09\/28\/2020", "09\/27\/2020"];
  // var dates = [{"checkin":"09\/25\/2020","checkout":"09\/29\/2020"},
  // {"checkin":"10\/02\/2020","checkout":"10\/10\/2020"},
  // {"checkin":"10\/10\/2020","checkout":"10\/14\/2020"}]
  function DisableDates(date) {
      var string = jQuery.datepicker.formatDate('mm/dd/yy', date);
      return [dates.indexOf(string) == -1];
  }
    var abc = $('#checkin').val();	
    var dateFormat = "mm/dd/yy";
    var date = new Date();
    var currentMonth = date.getMonth();
    var currentDate = date.getDate();
    var currentYear = date.getFullYear();
      from = $( "#checkin" )
        .datepicker({
          defaultDate: "+1d",
          changeMonth: true,
          changeYear: true,
          numberOfMonths: 3,
          maxDate: "+1y",
          beforeShowDay: DisableDates,
          minDate: new Date(currentYear, currentMonth, currentDate),  
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#checkout" ).datepicker({
        defaultDate: "+3d",
        changeMonth: true,
        numberOfMonths: 3,
        beforeShowDay: DisableDates,
        maxDate: "+1y",
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );

</script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> -->

<!-- <script>
var startDate, endDate, dateRange = [];

$("#CheckIn").datepicker({
    dateFormat : 'yy-mm-dd',
    onSelect: function (date) {
        startDate = $(this).datepicker("getDate");
    }
});

$("#CheckOut").datepicker({
    dateFormat : 'yy-mm-dd',
    onSelect: function (date) {
        endDate = $(this).datepicker("getDate");
        dateRange = [];
        for (var d = new Date(startDate);
            d <= new Date(endDate);
            d.setDate(d.getDate() + 1)) {
                dateRange.push($.datepicker.formatDate('yy-mm-dd', d));
                debugger;
        }
        $("#book").datepicker("destroy");
        $("#book").datepicker({ 
            dateFormat : 'yy-mm-dd',
            beforeShowDay: disableDates
        });
    }
});

$("#CheckIn, #CheckOut").datepicker("setDate", new Date());

$('#book').datepicker({
    dateFormat : 'yy-mm-dd',
    beforeShowDay: disableDates
});

var disableDates = function(dt) {
        var dateString = jQuery.datepicker.formatDate('yy-mm-dd', dt);
        return [dateRange.indexOf(dateString) == -1];
}
</script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<!-- <script>
  $(function(){

    // create new string
    var dateToday = new Date();
    
    // get month, day, year 
    var month = dateToday.getMonth() + 1;
    var day = dateToday.getDate();
    var minYear = dateToday.getFullYear();
    var maxYear = dateToday.getFullYear() + 1;

    // set month = 07 if month = 7
    if(month < 10)
        month = '0' + month.toString();

    // set day = 08 if day = 8
    if(day < 10)
        day = '0' + day.toString();  
    
    // set the year, month, day to complete string
    var maxDate = maxYear + '-' + month + '-' + day; 
    var minDate = minYear + '-' + month + '-' + day;
    // set the custom minDate to your input CheckIn
    $('#CheckIn').attr('min',minDate);
    
    // set custom maxDate to your input CheckOut
    $('#CheckOut').attr('max',maxDate);
    
    // set the function to checkout validation checkin date
    var checkinDate = $("#CheckIn").val();	
    $("#CheckIn").change(()=>{
      // debugger;
    if(checkinDate != null || checkinDate != "")
    {     
      var checkoutDate = $('#CheckIn').val();
      $('#CheckOut').attr('min',checkoutDate); 
    }
  });
</script>  -->
<!-- <script type="text/javascript">
//                   var nowTemp = new Date();
//                   var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
//                   var checkin = $('#dp1').datepicker({
//                     beforeShowDay: function(date) {
//                       return date.valueOf() >= now.valueOf();
//                     },
//                     autoclose: true
//                   }).on('changeDate', function(ev) {
//                     if (ev.date.valueOf() > checkout.datepicker("getDate").valueOf() || !checkout.datepicker("getDate").valueOf()) {

//                       var newDate = new Date(ev.date);
//                       newDate.setDate(newDate.getDate() + 1);
//                       checkout.datepicker("update", newDate);

//                     }
//                     $('#dp2')[0].focus();
//                   });
//                   var checkout = $('#dp2').datepicker({
//                     beforeShowDay: function(date) {
//                       if (!checkin.datepicker("getDate").valueOf()) {
//                         return date.valueOf() >= new Date().valueOf();
//                       } else {
//                         return date.valueOf() > checkin.datepicker("getDate").valueOf();
//                       }
//                     },
//                     autoclose: true

//                   }).on('changeDate', function(ev) {});

//               </script>          -->
