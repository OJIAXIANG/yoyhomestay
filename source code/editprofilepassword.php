<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show Password</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>
<?php
include('header.php');
include('dataconnection.php');  
if(isset($_SESSION['id']))
  {
      $id = $_SESSION['id'];      
  }
else if(isset($_SESSION['hostid'])){
  $id = $_SESSION['hostid'];
} 
else{
  $id = "";
}
?>
<body>
<?php echo $id ; ?>
<div class="container border shadow mt-5 w-25 d-flex justify-content-center">
    <h1><i class="fa fa-address-card-o w3-xxlarge w3-text-teal"></i>Edit Password</h1>
</div>
<div class="container mt-5 d-flex justify-content-center w-50 shadow-lg rounded">
    <div class="container mt-3">
      <form action="" name="changepassword" method="POST">
        <div class="row mt-3">      
          <div class="col">      
            <a style="font-size:25px">Old Password &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </a>
                  <input type="password" name="currentpwd" id="myInput" placeholder="Old Password"
                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
              <input type="checkbox" onclick="myFunction()">
          </div>    
        </div>
        <div class="row mt-3">      
          <div class="col">      
            <a style="font-size:25px">New Password &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :  </a>
              <input type="password"  name="newpwd" id="myInput2" placeholder="New Password"
              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
              <input type="checkbox" onclick="myFunction2()">
          </div>    
        </div>
        <div class="row mt-3"> 
          <div class="col">      
            <a style="font-size:25px">Repeat Password &nbsp;: </a>
              <input type="password" name="repnewpwd" id="myInput3" placeholder="Re-Password"
              pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}">
              <input type="checkbox" onclick="myFunction3()">
          </div>  
        </div>
      <button class="btn btn-info shadow mb-4 mt-4 btn-lg"  type="submit" name="submit" onclick="return check();">EDIT PASSWORD</button>
      <a href="profileguest.php" class="btn btn-outline-info btn-lg">GO BACK</a>
      </form>
    </div>
</div>
<?php
if(isset($_POST['submit'])){
  $curpass = $_POST['currentpwd'];
  $newpass = $_POST['newpwd'];
  $sql = "SELECT * from guest where Guest_ID='$id' and Guest_password='$curpass'";
  $result = mysqli_query($connect,$sql);
  $host = "SELECT * from host where Host_ID='$id' and Host_password='$curpass'";
  $reshost = mysqli_query($connect,$host);
  if(mysqli_num_rows($result)>0)
  {
    $sqlg = "UPDATE guest set Guest_password='$newpass' where Guest_password='$curpass' and Guest_ID='$id'";
    if(mysqli_query($connect,$sqlg))
    {
      ?>
      <script>      
            alert('New Password Updated!');        
            window.location.href = "profileguest.php";
     </script>
     <?php
    }
    else
    {
      echo "<script>alert('Your Old Password is Invalid!');</script>";
    }
  }
  else if(mysqli_num_rows($reshost)>0){
    $sqlg = "UPDATE host set Host_password='$newpass' where Host_password='$curpass' and Host_ID='$id'";
    if(mysqli_query($connect,$sqlg))
    {
      ?>
      <script>      
            alert('New Password Updated!');        
            window.location.href = "profilehost.php";
     </script>
     <?php
    }
    else
    {
      echo "<script>alert('Your Old Password is Invalid!');</script>";
    }
  }
  else
  {
    echo "<script>alert('Your Old Password is Invalid!');</script>";
  }
}
?>
<script>
function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

function myFunction2() {
  var y = document.getElementById("myInput2");
  if (y.type === "password") {
    y.type = "text";
  } else {
    y.type = "password";
  }
}

function myFunction3() {
  var z = document.getElementById("myInput3");
  if (z.type === "password") {
    z.type = "text";
  } else {
    z.type = "password";
  }
}

</script>
 

<script>
function check()
{
	var alt="";

	var v=document.forms["changepassword"]["newpwd"].value; 
      if (v==null || v=="")
      {
        alt += "New Password must be filled out\n";
        
      }
    
      var t=document.forms["changepassword"]["repnewpwd"].value; 
      if (t==null || t=="")
      {
        alt += "Must Re-enter New Password to change the Old Password\n";
        
      }
      if (v != t)
      {
        alt += "Your New Password and Re-Password doesn't match\n";
        
      }
	  if (alt != "")
      {
        alert(alt);
        return false;
      }
      else {
      form.Submit();
      alert("Password Changed Successfully!");
      }
}
</script>

</body>
</html>