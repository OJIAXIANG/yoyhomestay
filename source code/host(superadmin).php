<?php
include('header.php');
include('dataconnection.php');
?>
<DOCTYPE html>
<head>
    <title>List of Guests and Hosts</title>
    <link rel="stylesheet" type="text/css" href="adminmenu.css">

<style>
</style>
<script type="text/javascript">
//create a javascript function named confirmation()
function confirmation()
{
	var answer;
	answer=confirm("Are you sure you want to delete this host?");
	return answer;
}
</script>
</head>

<body>
	<div class="tab">
    <button class="tablinks" onclick="location.href='admin(delete).php'">Admin List</button>
    <button class="tablinks active">Hosts List</button>
    <button class="tablinks" onclick="location.href='homestay(superadmin).php'">Homestay List</button>
    <button class="tablinks" onclick="location.href='guest(superadmin).php'">Guest List</button>
    </div>
    <div class="containerjx">
		<h1>Lists of Hosts</h1>
    <table>

        <tr>
            <th>Host ID</th>
            <th>Host Name</th>   
            <th >Host Contact Number</th>
            <th>Host Email</th>
            <th>No. of Homestays Uploaded</th>
            <th style="width:10px; height:10px;">Profile Picture</th>
           
            
            
        </tr>

        <?php
			
			$result = mysqli_query($connect, "SELECT * from host");	
			$count = mysqli_num_rows($result);//used to count number of rows
            
            
			while($row = mysqli_fetch_assoc($result))
			{
                $id=$row['Host_ID'];
                $results = mysqli_query($connect, "SELECT * from approved_homestay WHERE Host_ID='$id'");	
                $counts = mysqli_num_rows($results);//used to count number of rows
			?>		

        <tr>

            <td>
                    <?php echo $row["Host_ID"]; ?>
                </td>
                <td> <?php echo $row["Host_N"]; ?></td>
                <td> <?php echo $row["Host_Contact"]; ?></td>
                <td> <?php echo $row["Host_email"]; ?></td>
                <td style="width:200px;"><?php echo $counts; ?></td>
                <td > 	
                <?php
                    $dir  ='profile/';
                    
                    // Image selection and display:
                   
                    echo "<img style='width:200px; height:200px;'src='$dir".$row['hostimage']."'>";
                    
                ?>
                </td>
               
                
            </tr>

                <?php
                }
                ?>
    </table>
    <p> Number of records : <?php echo $count; ?></p>
    </div>
</body>
</html>
<?php
if(isset($_GET["id"])) 
{
    $hostid=$_GET["id"];
    $sql ="DELETE from host WHERE Host_ID='$hostid'";
    if(!mysqli_query($connect,$sql)) {
        die('Error: ' . mysqli_error($connect));
    }

    else { 
		?>
		<script> alert("Success") ; window.location.href="host.php" </script>
        <?php
        
		}

	
}
?>