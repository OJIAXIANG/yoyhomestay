<!DOCTYPE html>
<head>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
  body{
    background-color:#4A6FA5;
    background-repeat:no-repeat;
    background-size:100%;
  }
#b1,#b2,#b3,#b4,#b5{
  font-size:45px;
  padding:60px;
  margin:10px;
  font-weight:bold;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
}

#b1:hover{
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.6), 0 17px 50px 0 rgba(0,0,0,0.4);
}
#b2:hover{
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.6), 0 17px 50px 0 rgba(0,0,0,0.4);
}
#b3:hover{
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.6), 0 17px 50px 0 rgba(0,0,0,0.4);
}
#b4:hover{
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.6), 0 17px 50px 0 rgba(0,0,0,0.4);
}
#b5:hover{
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.6), 0 17px 50px 0 rgba(0,0,0,0.4);
}

</style>
<body>
<script src="blue.js"></script>
<?php include("header.php");?>
    <h2 style="margin-left:20px; text-align:center; color:white;"> Hello!  Admin</h2>
    <table style="width:100%; margin:auto; padding:50px; text-align:center;">
      <tr>
        <td ><a id="b1" class="w3-button w3-padding-42 w3-border w3-border-black w3-hover-light-blue w3-round-xxlarge w3-hover-text-white" style="background-color:#DBE9EE;" href="verifyhomestay.php">Verify Homestay</a></td>
        <td></td>
        <td ><a id="b2" class="w3-button w3-padding-42 w3-border w3-border-black w3-hover-light-blue w3-round-xxlarge w3-hover-text-white" style="background-color:#DBE9EE;" href="homestaydetails.php">Homestay List</a></td>
      </tr>
      <tr>
        <td></td>
        <td ><a id="b4" class="w3-button w3-padding-42 w3-border w3-border-black w3-hover-light-blue w3-round-xxlarge w3-hover-text-white" style="background-color:#DBE9EE;" href="paymenthistory.php">Payment History</a></td>
        <td></td>
      </tr>
        <td ><a id="b4" class="w3-button w3-padding-42 w3-border w3-border-black w3-hover-light-blue w3-round-xxlarge w3-hover-text-white" style="background-color:#DBE9EE;" href="host.php">Hosts List</a></td>
        <td></td>
        <td ><a id="b3" class="w3-button w3-padding-42 w3-border w3-border-black w3-hover-light-blue w3-round-xxlarge w3-hover-text-white" style="background-color:#DBE9EE;" href="guest.php">Guests List</a></td>
      </tr>
    </ul>
</table>
</div>

</body>
</html>