<?php
include('header.php');
include('dataconnection.php');
?>
<DOCTYPE html>
<head>
    <title>List of Guests and Hosts</title>
    <link rel="stylesheet" type="text/css" href="adminmenu.css">
<style>
th
{
    background-color:#e7e7e7;
}
td
{
    background-color:white;
    text-align:center;
}
</style>
<script>
    function confirmation()
    {
        var answer;
        answer=confirm("Do you want to delete this guest?");
        return answer;
    }
</script>
</head>
<body>
<div class="tab">
    <button class="tablinks active" >Guests List</button>
    <button class="tablinks" onclick="location.href='host.php'">Hosts List</button>
    <button class="tablinks" onclick="location.href='homestaydetails.php'">Homestay List</button>
    <button class="tablinks" onclick="location.href='paymenthistory.php'">Payment History</button>
    </div>
    <div class="containerjx">
    <h1>Lists of Guests</h1>
        <table>
            <tr>  
                <th>Guest ID</th>
                <th>Guest Name</th>
                <th>Guest Contact Number</th>
                <th>Profile Picture</th>
                <th>Guest Email</th>  
                <th></th>     
            </tr>
            <?php		
                $result = mysqli_query($connect, "SELECT * from guest");       	
                $count = mysqli_num_rows($result);//used to count number of rows		
                while($row = mysqli_fetch_assoc($result))
                {		
                ?>			
            <tr>
                <td>
                    <?php echo $row["Guest_ID"]; ?>
                </td>
                <td> <?php echo $row["Guest_Name"]; ?></td>
                <td> <?php echo $row["Guest_contact"]; ?></td>
                <td>  
                <?php
                    $dir  ='profile/';
                    
                    // Image selection and display:
                    
                    echo "<img style='width:200px; height:200px;' src='$dir".$row['profileimage']."'>";
                   
                   
                    
                ?>
                </td>
                <td> <?php echo $row["Guest_email"]; ?></td>                      
                <td>
                    <a href="guest.php?id=<?php echo $row['Guest_ID'];?>" onclick="return confirmation()" style="display: inline-block; float:left; border-radius:15px; width:50px;">Delete</a>
                </td>
            </tr>
            <?php
                }		
                ?>
        </table>
        <p> Number of guest : <?php echo $count; ?></p>
    </div>
</body>
</html>
<?php
if(isset($_GET["id"])) 
{
    $guestid=$_GET["id"];
    $sql = "SELECT * from booking where Guest_ID='$guestid'";
    $result = mysqli_query($connect,$sql);
    if(!mysqli_num_rows($result)==1){
        $sql = "SELECT * from guest where Guest_ID='$guestid'";
        $result = mysqli_query($connect,$sql);
        $row = mysqli_fetch_array($result);
        $email = $row['Guest_email'];
        $name = $row['Guest_name'];
        $fromEmail = "yoyhomestay@gmail.com";
        $toEmail = $email;
        $subjectName = "Deletion of your account";
        $message = "You have not been active for 2 years and for that reason we had deleted your account.";
        $to = $toEmail;
        $subject = $subjectName;
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;" . "\r\n";
        $headers .= "From: ".$fromEmail."\r\n".
                    "Reply-To: ".$fromEmail."\r\n" . 
                    'X-Mailer: PHP/' . phpversion();
        $body = '<html>
                <body>
                <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$message.'</span>
                    <div class="container">
                        Dear '.$name.' ,<br>
                        '.$message.'<br>
                        Regards<br/>
                    '.$fromEmail.'
                    </div>
                </body>
                </html>';  
        if(mail($to, $subject, $body, $headers)){
            $del ="DELETE from guest WHERE Guest_ID='$guestid'";
            if(!mysqli_query($connect, $del)) {
                die('Error: ' . mysqli_error($connect));
            }
            else {   
            ?>
            <script> 
                alert("Guest has been deleted!") ; 
                window.location.href = "guest.php";
            </script>
            <?php 
            }
        }
    }
    else{ 
        while($row = mysqli_fetch_array($result))
        {   
            $getdate[] = array(
            'checkout' => $row['checkout']
            );     
        }
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date = date("m/d/Y");
        //start output
        $outout = "01/01/2020";
        for($x=0;$x<count($getdate);$x++)
        {
            $out = date_create($getdate[$x]['checkout']);
            $checkout =  date_format($out,"m/d/Y"); 
            if($outout<=$checkout)
            {
                $outout = $checkout;
            }
            else{
                $outout = $outout;
            }
        }
        // $outout is the biggest date
            if($outout<$date)
            {   $sql = "SELECT * from guest where Guest_ID='$guestid'";
                $result = mysqli_query($connect,$sql);
                $row = mysqli_fetch_array($result);
                $email = $row['Guest_email'];
                $name = $row['Guest_name'];
                $fromEmail = "yoyhomestay@gmail.com";
                $toEmail = $email;
                $subjectName = "Deletion of your account";
                $message = "You have not been active for 2 years and for that reason we had deleted your account.";
                $to = $toEmail;
                $subject = $subjectName;
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;" . "\r\n";
                $headers .= "From: ".$fromEmail."\r\n".
                            "Reply-To: ".$fromEmail."\r\n" . 
                            'X-Mailer: PHP/' . phpversion();
                $body = '<html>
                        <body>
                        <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$message.'</span>
                            <div class="container">
                                Dear '.$name.' ,<br>
                                '.$message.'<br>
                                Regards<br/>
                            '.$fromEmail.'
                            </div>
                        </body>
                        </html>';  
                if(mail($to, $subject, $body, $headers)){
                    $del ="DELETE from guest WHERE Guest_ID='$guestid'";
                    if(!mysqli_query($connect, $del)) {
                        die('Error: ' . mysqli_error($connect));
                    }
                
                    else {   
                    ?>
                    <script> 
                        alert("Guest has been deleted!") ; 
                        window.location.href = "guest.php";
                    </script>
                    <?php 
                    }     
                }
            }   
            else{
                ?>
                <script> 
                    alert("The guest's account cannot be deleted due to the guest is still booking the homestay or the account haven't exceed the 2 year inactive period.") ;
                    window.location.href = "guest.php"; 
                </script>
                <?php
            }          
             
	}
}
?>