<?php
require_once("dataconnection.php");
session_start();
require('fpdf182/fpdf.php');

$pdf = new FPDF();

$pdf->AddPage();

$logo = 'https://1.bp.blogspot.com/-qm43uQRZiyw/Xv2pXAA_twI/AAAAAAAAEWY/F0fn-oByr6cXnX-9OLcq_6btoXU4lRw_wCLcBGAsYHQ/s1600/logo.jpg';
$pdf->Image($logo);
$pdf->Ln(12);
$pdf->SetFont('Arial','b',20);

$pdf->Cell(55,5,'Payment History List',0,0);
$pdf->Ln(12);

$pdf->SetFont('Arial','b',8);

$pdf->Cell(16,5,'No',1);
$pdf->Cell(24,5,'Homestay Name',1,0);
$pdf->Cell(25,5,'Price per night',1,0);
$pdf->Cell(20,5,'Num of guest',1,0);
$pdf->Cell(20,5,'Check-in ',1,0);
$pdf->Cell(20,5,'Check-out',1,0);
$pdf->Cell(25,5,'Number of day',1,0);
$pdf->Cell(25,5,'Total Amount',1,0);
$pdf->Cell(20,5,'Guest Name',1,1);

if(isset($_POST['paypdf']))
{
    $result = mysqli_query($connect, "SELECT * from booking");      	
    $count = mysqli_num_rows($result);//used to count number of rows	
    while($r = mysqli_fetch_assoc($result))
    {
        $id=$r['Guest_ID'];
        $results = mysqli_query($connect, "SELECT * from guest WHERE Guest_ID='$id'");	
        if(!$results) {
            die('Error: ' . mysqli_error($connect));
        }
        else { 
            $s = mysqli_fetch_assoc($results);
            $resulta = mysqli_query($connect, "SELECT * from pay WHERE Guest_ID='$id'");	
            if(!$resulta) {
                die('Error: ' . mysqli_error($connect));
            }
            else { 
                $a = mysqli_fetch_assoc($resulta);
    $pdf->SetFont('Arial','',8);         
    $pdf->Cell(16,5,$r["book_id"],1,0);
    $pdf->Cell(24,5,$r["homestay_name"],1,0);
    $pdf->Cell(25,5,'RM '.$r["book_price"],1,0);
    $pdf->Cell(20,5,$r["num_guest"],1,0);
    $pdf->Cell(20,5,$r["checkin"],1,0);
    $pdf->Cell(20,5,$r["checkout"],1,0);
    $pdf->Cell(25,5,$r["num_days"],1,0);
    $pdf->Cell(25,5,'RM '.$a["totalprice"],1,0);
    $pdf->Cell(20,5,$s["Guest_Name"],1,1);
                }
        }
    }
}
$pdf->Output();
?>