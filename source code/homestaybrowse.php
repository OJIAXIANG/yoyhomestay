<!DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="scrollbar.css">
</head>
<style>
#box-column	{
	width:98%;
	border:5px solid grey;
	margin:10px;
    border-radius:20px;
    padding:10px;
   }
#menu-image img	{
	margin-top:30px;
    margin:10px;
	width:160px;
	height:120px;
   }
#menu-desc	{
	width:100%;
	padding:25px;
}
#menu-desc h4	{
	color: darkblue;
	text-shadow:0px 0px 10px #B7B7B7;
	font-size:18pt;
}
#menu-desc p	{
	font-size:0.9em;
 	font-style:italic;
  }
h1{
    color: darkblue;
	text-shadow:0px 0px 10px #B7B7B7;
    text-align:center;
}
#myInput {
  background-image: url('https://1.bp.blogspot.com/-PPoCQsT5uYw/X4KuwW8FAHI/AAAAAAAAFrY/mCUP4BQ6MvsUyJmLL6sAR6q68DKFGOPEQCLcBGAsYHQ/s32/search.jpg');
  background-position: 10px 12px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}
#myUL li a:hover:not(.header) {
  background-color: #eee;
}
</style>

</head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#shenghao div#box-column").filter(function() {
      $(this).toggle($(this).find('#menu-desc').text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<?php include("header.php"); ?>
<body>
	<h1>Recommended Homestay</h1>
	
	<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
	<div id="shenghao">
	<?php
		include("dataconnection.php");
		
		if($connect->connect_error)
		{
			die("Connection failed:".$conn->connect_error);
		}
		
		
		$sql = "SELECT * from approved_homestay";
		$result = $connect->query($sql);
		
		while($row = mysqli_fetch_assoc($result))
			{
		$_SESSION['homeid'] = $row['approved_id'];	
	?>

		<div id="box-column">
			<div id="menu-image">
			<?php
				$dir  ='imagepreview1/';
				echo "<img src='$dir".$row['image']."'>";
				echo "<img src='$dir".$row['image2']."'>";
			?>
			</div>
			<div id="menu-desc">
					<h4><a href="#"><?php echo $row["homestay_name"];?></a></h4>
					<p>
						<?php echo $row["address_line"];?>
					</p>
					<div id="price">
							RM <?php echo $row["price_per_night"];?>
					</div>
					<form action="" method="POST">
						<a href="book.php?home=<?php echo $_SESSION['homeid'];?>" type="submit" style="float:right; font-style:normal;
						font-weight:bold;
						background-color:darkblue;
						text-decoration:none;
						color:white;
						padding:5px 10px;">Book Now</a>
					</form>	
			</div>
		</div>

    <?php	
		
		}
?> 
	</div>
</body>
</html>
