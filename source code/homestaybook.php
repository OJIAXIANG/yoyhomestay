<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>  
<body>
<?php include("header.php"); 
    include("dataconnection.php");?> 
<div class="container border shadow mt-5 w-25 d-flex justify-content-center bg-white rounded">
    <h2><i class="fa fa-money w3-xxlarge w3-text-teal">&nbsp;</i>HOMESTAY BOOK LIST</h2>
</div>
<div class="container border shadow mt-5 w-100 d-flex justify-content-center bg-white rounded">
<table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Homestay Name</th>
        <th scope="col">Guest Name</th>
        <th scope="col">Guest contact</th>
        <th scope="col">Checkin-Date</th>
        <th scope="col">Checkout-Date</th>
        <th scope="col">Total-Price</th>
        <th scope="col">Number Of Nights</th>
        </tr>
    </thead>
<?php 
    $hostid = $_SESSION['hostid'];
    $dis = "Please Update Your Infomation.";
    $sql = "SELECT * from booking where Host_ID = '$hostid'";
    $result = $connect->query($sql);
    if(!$result) {
        die('Error: ' . mysqli_error($connect));
    }
    else
    {   $num = 1;
        while($row = mysqli_fetch_assoc($result))
        {         
        $homestay = $row['homestay_name'];
        $in = $row['checkin'];
        $out = $row['checkout'];
        $days = $row['num_days'];
        $guest = $row['Guest_ID'];
        $days = $row['num_days'];
        $sqll = "SELECT * from guest where Guest_ID='$guest'";
        if(!mysqli_query($connect,$sqll)) {
            die('Error: ' . mysqli_error($connect));
        }
        else
        {   $res = mysqli_query($connect,$sqll);
            $roww = mysqli_fetch_assoc($res);
            $guestname = $roww['Guest_Name'];
            $guestcontact = $roww['Guest_contact'];
            $sqla = "SELECT * from pay where Host_ID = '$hostid'";
            $resulta = $connect->query($sqla);
            if(!mysqli_query($connect,$sqla)) {
                die('Error: ' . mysqli_error($connect));
            }
            else
            { $rowa = mysqli_fetch_assoc($resulta);
                $total = $rowa['totalprice'];
        ?>
    <tbody>
        <tr>
        <td><?php echo $num; ?></td>
        <td><?php echo $homestay;?></td>
        <td><?php echo $guestname; ?></td>
        <td><?php echo $guestcontact; ?></td>
        <td><?php echo $in; ?></td>
        <td><?php echo $out; ?></td>
        <td>RM : <?php echo $total;?></td>
        <td><?php echo $days;?> nights</td>
        </tr>
        <?php 
         $num++;  
                    }
                }
            }
        }    
        ?>  
        <tr>
            <td colspan="9">
        <p style="color:red;">***The Host can view the booking from guest.</p>  
            </td>
         </tr>
    </tbody>    
    </table>
</div>
</body>  
