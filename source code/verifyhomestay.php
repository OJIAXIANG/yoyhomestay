
<!DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="scrollbar.css">
<style>
#box-column
	{width:47%;
	height: 80%;
	 border:5px solid grey;
	 float:left;
	 margin:20px;
   border-radius:20px;
   padding:10px;

   }
#menu-image img
	{margin-top:30px;
	margin:10px;
	 width:200px;
	 height:160px;
	float:left;
	
	border: 5px solid #ddd;
    border-radius: 5px;
    padding: 3px;
	margin-left:120px;
   }
#menu-desc
	{width:100%;
	 float:left;
	 padding:25px;}
#menu-desc h4
	{color: darkblue;
	 text-shadow:0px 0px 10px #B7B7B7;
	 font-size:18pt;}
#menu-desc p
	{font-size:0.9em;
 	 font-style:italic;
  }
#menu-desc a
	{font-style:normal;
	 font-weight:bold;
	 background-color:white;
	 text-decoration:none;
	 color:white;
	 padding:5px 10px;
   float:right;}
h1{
  color: darkblue;
	 text-shadow:0px 0px 10px #B7B7B7;
   text-align:center;
padding:50px;
}
#price{
  float:left;
}
.button{
    font-size:1.3em;
}
</style>

</head>

<meta name="viewport" content="width=device-width, initial-scale=1">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

<body>
<?php include("header.php"); ?>


 
<h1>List of pending approval Homestay</h1>
<?php
  	include("dataconnection.php");
	
	if($connect->connect_error)
	{
		die("Connection failed:".$conn->connect_error);
	}
	
	$_SESSION["hostid"]="";
	$sql = "SELECT * from upload_homestay";
	$result = $connect->query($sql);
	
	while($row = mysqli_fetch_assoc($result))
		{
?>
	

<div>

	<div id="box-column">
		<div id="menu-image">
		
		<?php 
			$dir  ='imagepreview1/';
			// Image selection and display:
			echo "<img src='$dir".$row['image']."'>";
			echo "<img src='$dir".$row['image2']."'>";
		
		?>
		</div>
		
		<div id="menu-desc">
			<h4><?php echo $row["homestay_name"];?></h4>
			<p>
				Address: <?php echo $row["address_line"];?>
				<?php echo $row["homestay_id"];?>
			</p>
			
        <div id="price">
			<h6>Price per night : RM<?php echo $row["price_per_night"];?> </h6>
        </div>
	  
        <div class="button">
		
				<p><a href="verifyhomestaydetail.php?id=<?php echo $row['homestay_id']; ?>" style="color: black" value="Details">Detail</a></p>
		
        </div>
		</div>
	</div>
<?php	
}
?>
</div>
<div style="clear:both">
</div>
</body>
