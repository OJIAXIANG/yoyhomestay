<?php
include('header.php');
include('dataconnection.php');
?>
<DOCTYPE html>
<head>
    <title>List of Guests and Hosts</title>
    <link rel="stylesheet" type="text/css" href="adminmenu.css">
<style>
</style>
<script>
    function confirmation()
    {
        var answer;
        answer=confirm("Do you want to delete this guest?");
        return answer;
    }
</script>
</head>
<body>
<div class="tab">
    <button class="tablinks" onclick="location.href='admin(delete).php'">Admin List</button>
    <button class="tablinks" onclick="location.href='host(superadmin).php'">Hosts List</button>
    <button class="tablinks" onclick="location.href='homestay(superadmin).php'">Homestay List</button>
    <button class="tablinks active" >Guest List</button>
    </div>
    <div class="containerjx">
		<h1>List of Guests</h1>
    <table>
        <tr>
            
            <th>Guest ID</th>
            <th>Guest Name</th>
            <th style="width:200px;">Profile Picture</th>
            <th>Guest Contact Number</th>
            <th colspan="2">Guest Email</th>
            
            
        </tr>

        <?php
			
            $result = mysqli_query($connect, "SELECT * from guest");
            	
			$count = mysqli_num_rows($result);//used to count number of rows
			
			while($row = mysqli_fetch_assoc($result))
			{
			
			?>			

        <tr>
            <td>
                <?php echo $row["Guest_ID"]; ?>
            </td>
            <td> <?php echo $row["Guest_Name"]; ?></td>
            <td > 	
                <?php
                    $dir  ='profile/';
                    
                    // Image selection and display:
                   
                    echo "<img style='width:200px; height:200px;'src='$dir".$row['profileimage']."'>";
                    
                ?>
            </td>
            <td> <?php echo $row["Guest_contact"]; ?></td>
            <td> <?php echo $row["Guest_email"]; ?></td>
            
            
            
        </tr>
        <?php
			
			}
			
			?>

    </table>
    <p> Number of records : <?php echo $count; ?></p>
</body>
</html>
<?php
if(isset($_GET["id"])) 
{
    $guestid=$_GET["id"];
    $sql ="DELETE from guest WHERE Guest_ID='$guestid'";
    if(!mysqli_query($connect,$sql)) {
        die('Error: ' . mysqli_error($connect));
    }

    else { 
		?>
		<script> alert("Success") ; window.location.href="guest.php" </script>
        <?php
        
		}

	
}
?>