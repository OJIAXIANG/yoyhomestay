<?php include("dataconnection.php"); ?>
<?php include("header.php"); ?>
<html>
<head><title>Movie List</title>
<link rel="stylesheet" type="text/css" href="adminmenu.css">
<script type="text/javascript">
//create a javascript function named confirmation()
function confirmation()
{
	var answer;
	answer=confirm("Do you want to delete this admin?");
	return answer;
}
</script>
</head>
<style>
</style>
<body>
	<div class="tab">
    <button class="tablinks active" >Admin List</button>
    <button class="tablinks" onclick="location.href='host(superadmin).php'">Hosts List</button>
    <button class="tablinks" onclick="location.href='homestay(superadmin).php'">Homestay List</button>
    <button class="tablinks" onclick="location.href='guest(superadmin).php'">Guest List</button>
    </div>
    <div class="containerjx">
		<h1>List of Admins</h1>
		<table>
			<tr>
				<th>Staff ID</th>
				<th>Admin Name</th>
				<th>Admin Email</th>
				<th>Admin Password</th>
				<th>Admin Contact</th>
				<th colspan="3">Action</th>  
			</tr>
			<!--copy the code from admin(edit) and add on the delete hyperlink-->
			<?php			
			$result = mysqli_query($connect, "SELECT * from admins");	
			$count = mysqli_num_rows($result);//used to count number of rows			
			while($row = mysqli_fetch_assoc($result))
			{
			?>			
			<tr>
				<td><?php echo $row["staff_id"]; ?></td>
				<td><?php echo $row["admin_name"]; ?></td>
				<td><?php echo $row["admin_email"]; ?></td>
				<td><?php echo $row["admin_password"]; ?></td>
				<td><?php echo $row["admin_contact"]; ?></td>
				<td><a href="admin(detail).php?id=<?php echo $row['admin_id']; ?>">More Details</a></td>
				<td><a href="admin_edit.php?id=<?php echo $row['admin_id']; ?>">Edit</a> </td>
				<td> <a href="admin(delete).php?id=<?php echo $row['admin_id']; ?>" onclick="return confirmation() "> Delete</a></td>
				
			</tr>
			<?php		
			}		
			?>
		</table>
		<p> Number of admins : <?php echo $count; ?></p>
		</div>

</body>
</html>
<?php

if (isset($_GET["id"])) 
{
	$aid=$_GET["id"];
	mysqli_query($connect,"DELETE from admins where admin_id='$aid'");
?>
<script>
	alert("You have successfully deleted the admin.");
		window.location.href="admin(delete).php "; //refresh the page
</script>
<?php
}
?>
