<?php
include('header.php');
include('dataconnection.php');
?>
<DOCTYPE html>
<head>
    <title>List of Homestays</title>
    <link rel="stylesheet" type="text/css" href="adminmenu.css">
<style>
</style>
<script type="text/javascript">
//create a javascript function named confirmation()
function confirmation()
{
	var answer;
	answer=confirm("Do you want to delete this homestay?");
	return answer;
}
</script>

</head>
<body>
<div class="tab">
    <button class="tablinks" onclick="location.href='admin(delete).php'">Admin List</button>
    <button class="tablinks" onclick="location.href='host(superadmin).php'">Hosts List</button>
    <button class="tablinks active" >Homestay List</button>
    <button class="tablinks" onclick="location.href='guest(superadmin).php'">Guest List</button>
    </div>
    <div class="containerjx">
    <h1>Lists of Homestays (Approved)</h1>
    <table >
        <tr>
            <th>Homestay ID</th>
            <th >Host ID</th>
            <th style="width:100px;" colspan="2">Pictures</th>
            <th>Homestay Name</th>
            <th>Homestay Address</th>         
            <th style="width:150px;">Price per night</th>
            <?php		
			$result = mysqli_query($connect, "SELECT * from approved_homestay");	
			$count = mysqli_num_rows($result);//used to count number of rows		
			while($row = mysqli_fetch_assoc($result))
			{		
			?>			
        <tr>
            <td>
                <?php echo $row["homestay_id"]; ?>
            </td>
            <td> <?php echo $row["Host_ID"]; ?></td>
            <td colspan="2"> 	
                <?php
                    $dir  ='imagepreview1/';
                    
                    // Image selection and display:
                    
                    echo "<img style='width:200px; height:200px;' src='$dir".$row['image']."'>";
                   
                    echo "<img style='width:200px; height:200px;'src='$dir".$row['image2']."'>";
                    
                ?>
            </td>
            <td> <?php echo $row["homestay_name"]; ?></td>
            <td> <?php echo $row["full_address"]; ?></td>
            <td style="width:60px;">RM <?php echo $row["price_per_night"];  ?></td>
          
            <!-- <td>
                <a href="homestaydetails.php?id=<?php echo $row['approved_id'];?>" onclick="return confirmation()" style="color:black;">Delete</a>
            </td> -->
        </tr>

        <?php
			
			}
			
			?>

      
    </table>
    <p> Number of records : <?php echo $count; ?></p>
    </div>
</body>
</html>
<?php
if(isset($_GET["id"])) 
{
    $aid=$_GET["id"];
    $sql ="DELETE from approved_homestay WHERE approved_id='$aid'";
    if(!mysqli_query($connect,$sql)) {
        die('Error: ' . mysqli_error($connect));
    }

    else { 
		?>
		<script> alert("Success") ; window.location.href="homestaydetails.php" </script>
        <?php
        
		}

	
}
?>

