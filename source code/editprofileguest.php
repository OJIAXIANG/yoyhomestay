<!DOCTYPE html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

<style>

</style>
</head>
<body>
<?php  
include("header.php");   
include("dataconnection.php"); 
$id = $_SESSION["id"];
$result = mysqli_query($connect, "select * from guest where Guest_ID = $id");
  if($row = mysqli_fetch_assoc($result)){
    $proimage = $row['profileimage'];
    $name = $row['Guest_Name'];
    $email = $row['Guest_email'];
    $contact = $row['Guest_contact'];
    $info = $row['guest_info'];
    $password = $row['Guest_password'];
  }
  else
  {
    $name = $email = $contact = $info = "";
  }
?>
<div class="container border shadow mt-5 w-50 d-flex justify-content-center bg-white rounded">
    <h2><i class="fa fa-address-card-o w3-xxlarge w3-text-teal">&nbsp;</i><h1>Update Profile</h1>
</div>
<form action="" method="POST" enctype="multipart/form-data">
    <div class="container border shadow mt-5 w-50 d-flex justify-content-center bg-white rounded">
      <div class="container justify-content-center">
        <div class="row mx-auto mt-5">
        <i class="fa fa-image fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
          <a style="font-size:25px">Profile Picture : </a>&nbsp;&nbsp;&nbsp;&nbsp;
          <?php 
              $dir  ='profile/';
              // Image selection and display:
              echo "<img name='profile' style='height:180px; width:200px;' src='$dir".$proimage."'>"; 
            ?>
                <input type="file" name="profile" accept="image/x-png,image/gif,image/jpeg,image/jpg">
        </div>  
        <div class="row mx-auto mt-3">
          <i class="fa fa-user-o fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
          <label for="name"><a style="font-size:25px"> Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;&nbsp;</a></label>
            <input type="text" style="font-size:25px" placeholder="Enter New Name" value="<?php echo $name ?>" name="name" readonly >
        </div>
        <div class="row mx-auto mt-3">    
          <i class="fa fa-envelope fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
          <a style="font-size:25px"> Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;&nbsp;</a>
            <input type="email" style="font-size:25px" placeholder="Enter New Email" value="<?php echo $email ?>" name="email" readonly>
        </div>
        <div class="row mx-auto mt-3">   
          <i class="fa fa-phone fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i> 
          <a style="font-size:25px"> Contact &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
          <input type="text" style="font-size:20px" placeholder="Enter Contact New Number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="<?php echo $contact ?>"name="contact">
              <br>
        </div>    
        <div class="row mx-auto mt-3">
            <i class="fa fa-address-card-o fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
            <a style="font-size:25px;"> About Me : &nbsp;&nbsp;&nbsp;</a>
            <input type="text" style="font-size:20px" name="info" size="30" value="<?php echo $info ?>"name="info">
        </div>  
        <div class="row mt-5">
          <div class="col">
                <button type="submit" name="submit" class="btn btn-outline-info btn-lg">Update Profile</button>
          </div>  
          <div class="col">
          <a href="editprofilepassword.php" class="btn btn-outline-info btn-lg" > Edit Password</a>
          </div>  
        </div>
        
            <p style="color:red;">If you dont want do change u may leave it.</p>  
      </div>
    </div>
</form>

</body>
</html>
<?php
if(isset($_POST["submit"]))
{
  // Get image name
  $image = $_FILES['profile']['name'];
  if($image != ''||$image != Null)
  {
    $image2=$id.'_'.$image;
  }
  else
  {
    $result = mysqli_query($connect, "select * from guest where Guest_ID = $id");
    $row = mysqli_fetch_assoc($result);
    $image2 = $row['profileimage'];
  }
	$name = $_POST["name"];
  $contact = $_POST["contact"]; 
  $info = $_POST['info'];
  $sql = "UPDATE guest SET Guest_Name='$name',Guest_contact='$contact',profileimage='$image2',guest_info='$info' where Guest_ID=$id" ;
  // image file directory
  $target = "profile/".basename($image2);
  // execute query
  mysqli_query($connect, $sql);
  if (move_uploaded_file($_FILES['profile']['tmp_name'], $target)) {
    echo "alert('Image uploaded successfully')";
  }else{
    $msg = "Failed to upload image";
  }

  ?>
  <script>
    alert("Profile Updated !<?php echo $image2 ?>");
    window.location.href="profileguest.php";
  </script>
  <?php
}	
?>
	
	
