<?php
include('header.php');
include('dataconnection.php');
?>
<DOCTYPE html>
<head>
    <title>Payment History</title>
    <link rel="stylesheet" type="text/css" href="adminmenu.css">

<style>
th
{
    background-color:#e7e7e7;
}
td
{
    background-color:white;
    text-align:center;
}
</style>
<script>
    function confirmation()
    {
        var answer;
        answer=confirm("Do you want to delete this guest?");
        return answer;
    }
</script>
</head>
<body>
    <div class="tab">
    
    <button class="tablinks" onclick="location.href='guest.php'">Guests List</button>
    <button class="tablinks" onclick="location.href='host.php'">Hosts List</button>
    <button class="tablinks" onclick="location.href='homestaydetails.php'">Homestay List</button>
    <button class="tablinks active">Payment History</button>
    </div>
    <div class="containerjx">
    <h1> Payment Histories</h1>
        <table> 
            <tr>            
                <th>Homestay Name</th>
                <th>Price per night</th>
                <th>Num of guest</th>
                <th>Check-in </th>
                <th>Check-out</th>
                <th>Number of days</th>
                <th style="width:150px;">Total Amount</th>            
                <th>Guest Name</th>            
            </tr>
        <?php		
                $result = mysqli_query($connect, "SELECT * from booking");      	
                $count = mysqli_num_rows($result);//used to count number of rows	
                while($row = mysqli_fetch_assoc($result))
                {
                    $id=$row['Guest_ID'];
                    $results = mysqli_query($connect, "SELECT * from guest WHERE Guest_ID='$id'");	
                    if(!$results) {
                        die('Error: ' . mysqli_error($connect));
                    }
                    else { 
                    $rows = mysqli_fetch_assoc($results);
                    $resulta = mysqli_query($connect, "SELECT * from pay WHERE Guest_ID='$id'");	
                    if(!$resulta) {
                        die('Error: ' . mysqli_error($connect));
                    }
                    else { 
                        $rowa = mysqli_fetch_assoc($resulta);                       
                ?>			
            <tr>             
                <td>
                    <?php echo $row["homestay_name"]; ?>
                </td>
                <td> <?php echo $row["book_price"]; ?></td>
                <td> <?php echo $row["num_guest"]; ?></td>
                <td> <?php echo $row["checkin"]; ?></td>
                <td> <?php echo $row["checkout"]; ?></td>
                <td> <?php echo $row["num_days"]; ?></td>
                <td> RM<?php echo $rowa["totalprice"]; ?></td>
                
                <td> <?php echo $rows["Guest_Name"]; ?></td>
            </tr>
            <?php
                    }
                    }
                }                
                ?>
        </table>
        <p> Number of records : <?php echo $count; ?></p>
        <form action="payhispdf.php" method="POST">
            <p><button name="paypdf" formtarget="_blank" style="padding: 5px 20px;font-size: 1em;" class='btn btn-outline-info btn-sm'>DOWNLOAD</button></p>
        </form>
    </div>
</body>
</html>
