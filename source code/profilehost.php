<!DOCTYPE html>
<html>
<title>YOY HOMESTAY</title>
<head>
<link rel="stylesheet" type="text/css" href="scrollbar.css">
</head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <meta name="description" content="">

    <title>User profile form requirement</title>

<style>
 
</style>
<body>
<?php include("header.php");
  	include("dataconnection.php");
	
    if($connect->connect_error)
    {
      die("Connection failed:".$conn->connect_error);
    }
    $host_id = $_SESSION['hostid'];
    $dis = "Please Update Your Infomation.";
    $sql = "SELECT * from host where Host_ID='$host_id'";
    $result = $connect->query($sql);
    if(mysqli_num_rows($result)>0)
    {
      $row = mysqli_fetch_assoc($result);
      $proimage = $row['hostimage'];
      $info = $row['host_info'];
      $name = $row['Host_N'];
      $contact = $row['Host_Contact'];
      $email = $row['Host_email'];
          ?>
<body>

<!-- Page Container -->
<div class="container border shadow mt-5 w-25 d-flex justify-content-center bg-white rounded">
    <h2><i class="fa fa-address-card-o w3-xxlarge w3-text-teal">&nbsp;</i>YOUR PROFILE</h2>
</div>
  <div class="container border shadow mt-5 w-50 d-flex justify-content-center bg-white rounded">
    <div class="container justify-content-center">
      <div class="row mx-auto mt-5">
      <i class="fa fa-image fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
        <a style="font-size:25px">Profile Picture : </a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php 
              $dir  ='profile/';
              // Image selection and display:
              echo "<img style='height:180px; width:200px;' src='$dir".$proimage."'>"; 
            ?>
      </div>  
      <div class="row mx-auto mt-3">
      <i class="fa fa-user-o fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
        <a style="font-size:25px">Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </a>
            <a style="font-size:25px">&nbsp;&nbsp;<?php echo $name; ?></a>  
      </div>
        <div class="row mx-auto mt-3">    
        <i class="fa fa-envelope fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
        <a style="font-size:25px">Email Address &nbsp;: </a>
            <a style="font-size:25px">&nbsp;&nbsp;<?php echo $email; ?></a>
        </div>
        <div class="row mx-auto mt-3">   
        <i class="fa fa-phone fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i> 
        <a style="font-size:25px">Contact&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </a>
            <a style="font-size:25px">&nbsp;&nbsp;<?php echo $contact; ?></a>
            <br>
        </div>    
      <div class="row mt-3">
        <div class="col"> 
          <i class="fa fa-address-card-o fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>
          <a style="font-size:25px;">About Me &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </a>            
              <a style="font-size:25px">&nbsp;<?php if($info!="" && $info != NULL){echo $info; }else{?><a style="color:gray;"> <?php echo $dis;}?></a></a>
            <!-- if($row['guest_info']=""){echo $dis; }else{echo $row['guest_info'];} -->
        </div>  
      </div>  
      <div class="row mt-5">
        <div class="col">
          <form action="editprofilehost.php">
              <button class="btn btn-outline-info btn-lg">Edit Profile</button>
          </form>
        </div>  
        <div class="col">
          <form action="homestaybook.php">
              <button class="btn btn-outline-info btn-lg">Transaction</button>
          </form>
        </div>  
      </div>
          <p style="color:red;">If can you may edit your profile for more information.</p>  
   </div>
  </div>
<?php
    }
?>

</body>
</html>
