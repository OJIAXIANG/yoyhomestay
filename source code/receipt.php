<!DOCTYPE html>
<head>
</head>
<style>
.container{
    margin-top:80px;
}
</style>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<body>
<?php include("header.php"); ?>
<?php 
if(isset($_SESSION['id']))
{
    $id = $_SESSION['id'];
}
{
    $id = "";
}

    include("dataconnection.php");
    if($connect->connect_error)
    {
      die("Connection failed:".$conn->connect_error);
    }
    $homestayid = $_SESSION['homeid'];
    $home = $_SESSION['home'];
    $sql = "SELECT * from approved_homestay where approved_id = '$homestayid'";
    $result = $connect->query($sql);
    $sqlb = "SELECT * from booking where active = '1'";
    $resultb = $connect->query($sqlb);
    $sqlc = "SELECT * from pay where active = '1'";
    $resultc = $connect->query($sqlc);

    if(mysqli_num_rows($result)== 1)
    {
      $row = mysqli_fetch_assoc($result);
      $rowb = mysqli_fetch_assoc($resultb);
      $rowc = mysqli_fetch_assoc($resultc);
	
?>  
<div class="container">
    <div class="row">
        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <b style="font-size:20px;"><?php echo $home;?></b>
                        <div style="font-size:15px;"><?php echo $row['full_address'];?></div>
                    </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                        <?php date_default_timezone_set("Asia/Kuala_Lumpur"); ?>
                        <em><?php echo "Today is : " . date("Y-m-d h:i:s") . "<br>";?></em>
                    </p>
                    <p>
                        <em>Receipt #: <?php echo $rowb['book_id'];?></em>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                    <h1>Receipt</h1>
                </div>
                </span>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>The Status</th>
                            <th>#</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-9"><em>Check-In</em></h4></td>
                            <td colspan="3" class="col-md-1" style="text-align: left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $rowb['checkin'];?></td>                       
                        </tr>
                        <tr>
                            <td class="col-md-9"><em>Check-Out</em></h4></td>
                            <td colspan="3" class="col-md-1" style="text-align: left">Date :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $rowb['checkout'];?></td>             
                        </tr>
                        <tr>
                            <td class="col-md-9"><em>Number of Guest</em></h4></td>
                            <td colspan="3" class="col-md-1" style="text-align: center"><?php echo $rowb['num_guest'];?> People</td>
                        </tr>
                        <tr>
                            <td class="col-md-9"><em>Number of Nights Stayed</em></h4></td>
                            <td colspan="3" class="col-md-1" style="text-align: center"><?php echo $rowb['num_days'];?>  Nights</td>
                        </tr>                       
                        <tr>
                            <td class="col-md-9"><em>Per Days Price</em></h4></td>
                            <td colspan="3" class="col-md-1" style="text-align: center">RM <?php echo $rowb['book_price'];?></td>
                        </tr>
                        
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right">
                            <p>
                                <strong>Subtotal: </strong>
                            </p>
                            </td>
                            <td class="text-center">
                            <p>
                                <strong>RM<?php echo $rowb['book_price'];?> * <?php echo $rowb['num_days'];?> Nights</strong>
                            </p>
                            </td>
                        </tr>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td class="text-right"><h4><strong>Total: </strong></h4></td>
                            <td class="text-center text-danger"><h4><strong>RM<?php echo $rowc['totalprice'];?></strong></h4></td>
                        </tr>
                    </tbody>   
                </table>
                <form action="#" method="POST">
                        <div class="col mr-5">
                            <button type="submit" name="submit" class="btn btn-success btn-lg btn-block">Please Click To Complete Your Payment&#10003;</button>
                        </div>  
                 </div> 
                 </form>          
                        <div class="row mr-5 mt-5">
                            <div class="col mt-5">
                            <form action="pdf.php" method="POST">
                                <button name="pdf" formtarget="_blank" class="btn btn-outline-info btn-lg">Print Your Receipt</button>
                            </form>
                            </div>  
                        </div>
                
            
           
        </div> 
    </div>
   
<?php } ?>
</body>
</html>
<?php if(isset($_POST['submit'])){
    $sql = "UPDATE booking set active = '0' where active = '1' ";
    $sqla = "UPDATE pay set active = '0' where active = '1' ";
    if(!mysqli_query($connect, $sql)) {
        die('Error: ' . mysqli_error($connect));
    }
    else { if(!mysqli_query($connect, $sqla)) {
        die('Error: ' . mysqli_error($connect));
    }
    else { 
		?>
        <script> 
            alert("Your Payment is Completed Succesfully") ; 
            window.location.href = "index.php" ;
        </script>
		<?php
		}
    }
}