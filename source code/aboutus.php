<!DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="scrollbar.css">
<?php 
  include("header.php");?>
</head>
<style>
body {
  margin: 0;
  padding:0;
  background-color: #FEFFFF;
  font-family: "Open Sans", "Helvetica Neue", sans-serif;
}

/* a { */
	/* Typography Declrations */
	/* font-weight: bold;
	text-decoration: none;
	color: #D1DBBD; */
/* } */

/* a:hover { */
	/* Tyopography Declarations */
	/* color: #FCFFF5;
} */

/* a:visited { */
	/* Typography Declarations */
	/* color: #D1DBBD;
} */

h1 {
  color: #3E606F;
}

h2 {
  color: #193441;
}
.masthead-heading,
.masthead-intro {
  text-align: center;
  color: #FFF;
}
.masthead {
  padding: 6em 0;
  background-image: url('https://bit.ly/1Yf2Psj');/*Can Change BG IMF from here*/
  background-color: #1FAAFF;
  background-size: cover;
  background-repeat: no-repeat;
  
}
.masthead-intro {
  margin-bottom; 0.1em;
  font-family: "Gentium Book Basic", Georgia, serif;
  font-size: 2em;
}
.masthead-heading {
  margin-top: -0.2em;
  font-family: "Open Sans", "Helvetica Neue", sans-serif;
  font-weight: bold;
  font-size: 6em;
  letter-spacing: -0.02em;
  text-transform: uppercase;
}

.introduction-section, 
.image-section{
  max-width: 38em;
  margin-left: auto;
  margin-right: auto;
  margin-top: 2em;
}
.image-section{
  max-width: 50em	;
  margin-left: auto;
  margin-right: auto;
  margin-top: 2em;
}
.images {
	display: flex;
	list-style: none;
	padding: 0;
	text-align: center;
    color: #FEF;
}
.images > li:first-child{
	padding-left:0
}
.images > li{
	padding: 0 10px;
}
.images > li:last-child{
	padding-right:0
}
.img-responsive {
    width: 100%;
    height: auto;
}

.profile_container:hover::after {
  left: 150%;
  transition: all 1s;
  -webkit-transition: all 0.75s;
  -moz-transition: all 0.75s;
  -o-transition: all 0.75s;
  -ms-transition: all 0.75s;
}

.profile_container:hover .bigimg-overlay,
.profile_container:hover .bigimg-text {
	opacity: 1;
}
.bigimg-overlay {
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	height: 100%;
	width: 100%;
	opacity: 0;
	transition: 0.5s ease-in-out;
	background-image: linear-gradient(15deg, rgba(19, 84, 122, 0.7) 0%, rgba(128, 208, 199, 0.7) 100%);
}

.bigimg-text {
  color: #ffffff;
  opacity:0;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}

.profile_container {
  position: relative;
  overflow: hidden;
}

.profilepic {
  /* width: 100% !important; */
  height: auto;
  display: block;
  width:270px;
  height:280px;
}

footer {
  text-align: center;
  background-color: #36647F;
  color: #FFFFFF;
  padding: 2em;
}

.social {
  text-align: center;
  color: #FEF;
  padding: 10px;
}


.social > li {
  display: inline-block;
  margin: 5px;
  text-align: center;
 }
footer p {
  font-weight: 300;
  letter-spacing: 0.05em;
}

.side-padding {
    /* Layout Declarations */
    padding-left: 2em;
    padding-right: 2em;
}

</style>

<header class="masthead">
	<p class="masthead-intro">Hi! We are</p>
	<h1 class="masthead-heading">YoY</h1>  
</header>

<section class="image-section">
	<ul class="images">
		<li>
			<div class="profile_container">
				<img class="profilepic img-responsive" src="https://1.bp.blogspot.com/-mxNL5cJ-imo/X3ngRfOgeNI/AAAAAAAAFqo/vtwxGVwfImE6R8GpBl0Ba-eJRVr_an8awCLcBGAsYHQ/s750/1b72271d-6064-40fb-bd7a-c913f2c7c37f.jpg" alt="JiaXiang" />
				<div class="bigimg-overlay">
				  <div class="bigimg-text"><strong>YAP TUNG QUAN</strong><br>yaptungquan@gmail.com<br>01136692463</div>
				</div>
			</div>
		</li>
		<li>
			<div class="profile_container">
				<img class="profilepic img-responsive" src="https://1.bp.blogspot.com/-nUtQWcvRzXw/X3ngRVVEuRI/AAAAAAAAFqg/fLOMeNvvIAI5zj0xunY9bC3fbYDbWQc-QCLcBGAsYHQ/s813/jia%2Bxiang.JPG" alt="JiaXiang" />
				<div class="bigimg-overlay">
				  <div class="bigimg-text"><strong>ONG JIA XIANG</strong><br>xiang_0408@hotmail.com<br>0146125242</div>
				</div>
			</div>
		</li>
		<li>
			<div class="profile_container">
				<img class="profilepic img-responsive" src="https://1.bp.blogspot.com/-1Vwxw_dqh68/X3ngRRelFYI/AAAAAAAAFqk/b46ZQXkpn8I-9X4SbPrjTdVQQKVScMo2ACLcBGAsYHQ/s411/yi%2Bfeng.JPG" alt="JiaXiang" />
				<div class="bigimg-overlay">
				  <div class="bigimg-text"><strong>YEOW YI FENG</strong><br>yeowyifeng98@gmail.com<br>0126863719</div>
				</div>
			</div>
		</li>
	</ul>
</section>

<section class="introduction-section">
	<h1>About US</h1>
	<p>This is an online website created by 3 IT students from Multimedia University by the company name of YoY HomeStay.  
	This is the latest online booking homestay where the homestays are only available in Melaka. 

	<p>This creation of website is to give the guests or tourists the best temporary accommodation depending on the number of days that the guests has booked and for them
	to relax their minds, thinking about which location or the best local restaurant that they should visit, and experience various types of local cultures and customs.</p>
</section>



<!-- <footer class="site-padding">
	<p>Say hi to me via social media</p>
	<ul class="social">
		<li><a href="#">Github</a></li>
		<li><a href="#">Twitter</a></li>
		<li><a href="#">Google+</a></li>
	</ul>
</footer> -->
