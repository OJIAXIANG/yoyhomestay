<?php
require_once("dataconnection.php");
session_start();
require('fpdf182/fpdf.php');


if(isset($_POST['pdf']))
{
    $result = mysqli_query($connect,"SELECT * FROM booking where active='1'");
    $a = mysqli_fetch_assoc($result);
    $homestayid = $_SESSION['homeid'];
    $home = $_SESSION['home'];
    $res = mysqli_query($connect,"SELECT * from approved_homestay where approved_id = '$homestayid'");
    $b = mysqli_fetch_assoc($res);
    $resc = mysqli_query($connect,"SELECT * from pay where active='1'");
    $c = mysqli_fetch_assoc($resc);
    $pdf = new FPDF();

$pdf->AddPage();

$logo = 'https://1.bp.blogspot.com/-qm43uQRZiyw/Xv2pXAA_twI/AAAAAAAAEWY/F0fn-oByr6cXnX-9OLcq_6btoXU4lRw_wCLcBGAsYHQ/s1600/logo.jpg';
$pdf->Image($logo);
$pdf->SetFont('Arial','B',18);
$pdf->Ln(10);

$pdf->Cell(120,10,$a['homestay_name'],0,0);
$pdf->SetFont('Arial','',14);
date_default_timezone_set("Asia/Kuala_Lumpur");
$pdf->Cell(80,10,'Today is : '.date("Y-m-d h:i:s"),0,1);//endline 

$pdf->SetFont('Arial','',14);
$pdf->Cell(150,10,'Full Address : ',0,0);
$pdf->Cell(25,10,'Receipt # : ',0,0);
$pdf->Cell(20,10,$a['book_id'],0,1);

$pdf->Cell(55,10,$b['full_address'],0,1);
$pdf->Ln(12);

$pdf->SetFont('Arial','B',26);
$pdf->Cell(80,10,'',0,0);
$pdf->Cell(120,10,'Receipt',0,1);
$pdf->Ln(10);

$pdf->SetFont('Arial','b',14);
$pdf->Cell(100,10,'The Status ',0,0);
$pdf->Cell(20,10,'# ',0,0);
$pdf->Cell(40,10,'Price ',0,0);
$pdf->Cell(60,10,'Total ',0,1);

$pdf->SetFont('Arial','',14);
$pdf->Cell(100,10,'Check-In',0,0);
$pdf->Cell(28,10,'Date :',0,0);
$pdf->Cell(100,10,$a['checkin'],0,1);
$pdf->Cell(100,10,'Check-Out',0,0);
$pdf->Cell(28,10,'Date :',0,0);
$pdf->Cell(100,10,$a['checkout'],0,1);
$pdf->Cell(130,10,'Number of Guest',0,0);
$pdf->Cell(5,10,$a['num_guest'],0,0);
$pdf->Cell(100,10,' People',0,1);
$pdf->Cell(130,10,'Number of Days Stayed',0,0);
$pdf->Cell(5,10,$a['num_days'],0,0);
$pdf->Cell(100,10,' Days',0,1);
$pdf->Cell(120,10,'Per Days Price',0,0);
$pdf->Cell(10,10,'RM ',0,0);
$pdf->Cell(20,10,$a['book_price'],0,1);

$pdf->SetFont('Arial','b',16);
$pdf->Cell(110,10,'',0,0);
$pdf->Cell(40,10,'Subtotal : ',0,0);
$pdf->Cell(10,10,'RM',0,0);
$pdf->Cell(15,10,$a['book_price'],0,0);
$pdf->Cell(5,10,'*',0,0);
$pdf->Cell(100,10,$a['num_days'],0,1);

$pdf->SetFont('Arial','b',22);
$pdf->Cell(110,10,'',0,0);
$pdf->Cell(40,10,'Total : ',0,0);
$pdf->Cell(15,10,'RM',0,0);
$pdf->Cell(30,10,$c['totalprice'],0,1);

ob_end_clean();
$pdf->Output();
}

?>