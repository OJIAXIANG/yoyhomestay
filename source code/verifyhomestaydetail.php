<!DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="scrollbar.css">
<style>
#menu-image img
{
 mx-auto p-auto mt-3 d-flex justify-content-center
}
</style>
</head>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">


<body>
<?php include("header.php"); ?>
  <div class="container border shadow mt-3 w-25 d-flex justify-content-center">
    <h1>Detail of the Homestay</h1>
  </div>
<?php
  	include("dataconnection.php");
	//add
	if(isset($_GET["id"]))
	{	//add
		$homestay_id=$_GET["id"];
		
	}else
	{
		$homestay_id="";
		
	}
	
	if($connect->connect_error)
	{
		die("Connection failed:".$conn->connect_error);
	}
	//add
	$sql = "SELECT * from upload_homestay where homestay_id = '$homestay_id'";
	$result = $connect->query($sql);
	//take result to display
	$row=mysqli_fetch_assoc($result);
	
	
		
?>	
<div>

	<div class="container  mt-3 mb-5 d-flex justify-content-center w-85 shadow-lg">
		<div class="container">
		<form action="#" method="POST">
		
		
			<div class="row mt-5 align-items-center"> 
				<div class="col">  
					<?php
						$dir  ='imagepreview1/';
						// Image selection and display:
						
						
						
						echo "<img style='width:300px; height:250px;' src='$dir".$row['image']."' >";
					?>
					
				</div>
					<div class="col " style="font-size:20px">
						<?php echo $row["image_text"]; ?>
					</div>
				<div class="col">	
					<?php
						$dir  ='imagepreview1/';
						
						echo "<img style='width:300px; height:250px;' src='$dir".$row['image2']."' >";
					?>
					
				</div>
				
				
		
		<div class="col" style="font-size:20px">
				<?php echo $row["image_text2"];?>
			</div>
		</div>
	
			
		
		<div class="row mt-5">  
            <div class="col" style="font-size:30px"> 
				<p>Homestay Title:  <?php echo $row["homestay_name"];?></p>
			</div>
        </div> 
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px"> 
				<p>
					<p>About the homestay:  <?php echo $row["about_homestay"];?><br></input></p>
				</p>
			</div>
        </div>
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px">
				<p>
					<p>Address Line:  <?php echo $row["address_line"];?><br></input>
				</p>
		    </div>
        </div> 
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px">		
				<p>
					<p>Full Address:  <?php echo $row["full_address"];?></p>
				</p>
			</div>
        </div>

		<div class="row mt-1">  
            <div class="col" style="font-size:20px">	
				<p>
					<p>House Type:   <?php echo $row["house_type"];?></p>
				</p>
			</div>
        </div>
		
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px">
				<p>
					<p>Number of room in a homestay: <?php echo $row["number_of_room"];?></p>
				</p>
			</div>
        </div>
		
		<div class="row mt-1">  
            <div class="col" style="font-size:20px">
				<p>
					<p>Price per night : RM<?php echo $row["price_per_night"];?> </p>
				</p>
			</div>
        </div>
		
		<div class="row mt-3 mb-3">  
            <div class="col">
				<div class="button">
					<button class="approve_btn btn btn-outline-info btn-m"  name="approve" >Approve</button>
					<button class="reject_btn btn btn-outline-info btn-m" name="reject" >Reject</a>
				</div>
			</div>
        </div>
		
		</form>
		
	</div>
	
   <?php	
		
	
?>  

</div>


<?php

	
// Create database connection
include("dataconnection.php");

// Initialize message variable
$msg = "";

// If upload button is clicked ...
if(isset($_POST['approve'])) 
{
	  // Get image name
	 $image = $row['image'];
	 $image2 = $row['image2'];
	 	// Get text
  	$image_text = mysqli_real_escape_string($connect, $row['image_text']);
	$image_text2 = mysqli_real_escape_string($connect, $row['image_text2']);
	$homestay_id=$row['homestay_id'];
	$homestay_name=$row['homestay_name'];
	$about_homestay=$row['about_homestay'];
	$house_type=$row['house_type'];
	$address_line=$row['address_line'];
	$full_address=$row['full_address'];
	$number_of_room=$row['number_of_room'];
	$price_per_night=$row['price_per_night'];
	$hostid=$row['Host_ID'];	
	//same name because data from the database
	$sql = "INSERT INTO approved_homestay (image,image2,image_text,image_text2,homestay_id,homestay_name,about_homestay,house_type,address_line,full_address,number_of_room,price_per_night,Host_ID) VALUES ('$image','$image2', '$image_text', '$image_text2','$homestay_id','$homestay_name','$about_homestay','$house_type','$address_line','$full_address','$number_of_room','$price_per_night','$hostid')";
	// execute query
  	mysqli_query($connect, $sql);
	mysqli_query($connect,"DELETE from upload_homestay where homestay_id='$homestay_id'");
	$sqll = "SELECT * from host where Host_ID = '$hostid'";
	if(!mysqli_query($connect, $sqll)) {
        die('Error: ' . mysqli_error($connect));
    }
	else 
	{ 
		$result = mysqli_query($connect,$sqll);
		$row = mysqli_fetch_assoc($result);
		$hostname = $row['Host_N'];
		$hostemail = $row['Host_email'];
		$fromEmail = "yoyhomestay@gmail.com";
		$toEmail = $hostemail;
		$subjectName = "Your Homestay has been Approved";
		$message = 'Hello!! Dear '.$hostname.' your Homestay has been approved !! Thank you for your uploaded homestay.';
		$to = $toEmail;
		$subject = $subjectName;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;" . "\r\n";
		$headers .= "From: ".$fromEmail."\r\n".
					"Reply-To: ".$fromEmail."\r\n" . 
					'X-Mailer: PHP/' . phpversion();
		$body = '<html>
				<body>
				<span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$message.'</span>
					<div class="container">
						'.$message.'<br><br>
						Regards<br/>
					'.$fromEmail.'
					</div>
				</body>
				</html>';  
		if(mail($to, $subject, $body, $headers)){
		?>
		<script>
			alert("The homestay has been approve");
			window.location.href = "verifyhomestay.php";
		</script>
		<?php
		}
	}
}
if(isset($_POST['reject']))
{
	$hostid=$row['Host_ID'];	
	$sqll = "SELECT * from host where Host_ID = '$hostid'";
	if(!mysqli_query($connect, $sqll)) {
        die('Error: ' . mysqli_error($connect));
    }
	else 
	{ 
		$result = mysqli_query($connect,$sqll);
		$row = mysqli_fetch_assoc($result);
		mysqli_query($connect,"DELETE from upload_homestay where homestay_id='$homestay_id'");
		$hostname = $row['Host_N'];
		$hostemail = $row['Host_email'];
		$fromEmail = "yoyhomestay@gmail.com";
		$toEmail = $hostemail;
		$subjectName = "Your Homestay has been Rejected";
		$message = 'Hello!! Dear '.$hostname.' your Homestay has been rejected .';
		$to = $toEmail;
		$subject = $subjectName;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;" . "\r\n";
		$headers .= "From: ".$fromEmail."\r\n".
					"Reply-To: ".$fromEmail."\r\n" . 
					'X-Mailer: PHP/' . phpversion();
		$body = '<html>
				<body>
				<span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$message.'</span>
					<div class="container">
						'.$message.'<br>
						Please upload your homestays base on the requirements : <br>
						a) Price per night at least RM300 or above.<br>
						b) Pictures of the homestay must be related.<br>
						c) Address of the homestay must be within Melaka.<br>
						d) Description of the homestay must be appropriate.<br>
						e) Please select the type of house correctly.<br>
						Regards<br/>
					'.$fromEmail.'
					</div>
				</body>
				</html>';  
		if(mail($to, $subject, $body, $headers)){
		?>
		<script>
			alert("The homestay has been reject");
			window.location.href = "verifyhomestay.php";
		</script>
		<?php
		}	
	} 
}
?>  

<div style="clear:both">
</div>
</body>
