<?php
include('header.php');
include('dataconnection.php');
?>
<DOCTYPE html>
<head>
    <title>List of Guests and Hosts</title>
    <link rel="stylesheet" type="text/css" href="adminmenu.css">
<style>
td
{
    background-color:white;
    text-align:center;
}
th
{
    background-color:#e7e7e7;
}
</style>
<script type="text/javascript">
//create a javascript function named confirmation()
function confirmation()
{
	var answer;
	answer=confirm("Are you sure you want to delete this host?");
	return answer;
}
</script>
</head>
<body>
<div class="tab">
    
    <button class="tablinks" onclick="location.href='guest.php'">Guests List</button>
    <button class="tablinks active" >Hosts List</button>
    <button class="tablinks" onclick="location.href='homestaydetails.php'">Homestay List</button>
    <button class="tablinks" onclick="location.href='paymenthistory.php'">Payment History</button>
    </div>
    <div class="containerjx">
    <h1>Lists of Hosts</h1>
    <table>
        <tr>   
            <th>Host ID</th>
            <th>Host Name</th>
            <th>Host Contact Number</th>
            <th>Host Email</th>
            <th>Profile Picture</th>
            <th colspan="2">No. of Homestays</th>
            <th></th>
            <th></th>
        </tr>
        <?php		
			$result = mysqli_query($connect, "SELECT * from host");	
			$count = mysqli_num_rows($result);//used to count number of rows       
			while($row = mysqli_fetch_assoc($result))
			{
                $id=$row['Host_ID'];
                $results = mysqli_query($connect, "SELECT * from approved_homestay WHERE Host_ID='$id'");	
                $counts = mysqli_num_rows($results);//used to count number of rows
			?>		
        <tr>
            <td>
                <?php echo $row["Host_ID"]; ?>
            </td>
            <td> <?php echo $row["Host_N"]; ?></td>
            <td> <?php echo $row["Host_Contact"]; ?></td>
            <td> <?php echo $row["Host_email"]; ?></td>
            <td>  
                <?php
                    $dir  ='profile/';
                    
                    // Image selection and display:
                    
                    echo "<img style='width:200px; height:200px;' src='$dir".$row['hostimage']."'>";
                   
                   
                    
                ?></td>
            <td style="width:200px;"><?php echo $counts; ?></td>
            <td>
            <a href="host.php?id=<?php echo $row['Host_ID'];?>" onclick="return confirmation()">Delete</a>
            </td>    
        </tr>

                <?php
                }
                ?>
    </table>
    <p> Number of records : <?php echo $count; ?></p>
    </div>
</body>
</html>
<?php
if(isset($_GET["id"])) 
{
    $hostid=$_GET["id"];
    $sql = "SELECT * from booking where Host_ID='$hostid'";
    $result = mysqli_query($connect,$sql);
    if(!mysqli_num_rows($result)==1){
        $sql = "SELECT * from host where Host_ID='$hostid'";
        $result = mysqli_query($connect,$sql);
        $row = mysqli_fetch_array($result);
        $email = $row['Host_email'];
        $name = $row['Host_name'];
        $fromEmail = "yoyhomestay@gmail.com";
        $toEmail = $email;
        $subjectName = "Deletion of your account";
        $message = "You have not been active for 1 year and for that reason we had deleted your account and you will not be able to continue your business in our website.";
        $to = $toEmail;
        $subject = $subjectName;
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;" . "\r\n";
        $headers .= "From: ".$fromEmail."\r\n".
                    "Reply-To: ".$fromEmail."\r\n" . 
                    'X-Mailer: PHP/' . phpversion();
        $body = '<html>
                <body>
                <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$message.'</span>
                    <div class="container">
                        Dear '.$name.' ,<br>
                        '.$message.'<br>
                        Thank you very much.
                        Regards<br/>
                    '.$fromEmail.'
                    </div>
                </body>
                </html>';  
        if(mail($to, $subject, $body, $headers)){
        $sql ="DELETE from host WHERE Host_ID='$hostid'";
                if(!mysqli_query($connect, $sql)) {
                    die('Error: ' . mysqli_error($connect));
                }
            
                else {   
                ?>
                <script> 
                    alert("Host is already deleted!") ; 
                    window.location.href = "host.php";
                </script>
                <?php 
                }     
        }
    }
    else{ 
        while($row = mysqli_fetch_array($result))
        {   
            $getdate[] = array(
            'checkout' => $row['checkout']
            );     
        }
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date = date("m/d/Y");
        //start output
        $outout = "01/01/2020";
        for($x=0;$x<count($getdate);$x++)
        {
            $out = date_create($getdate[$x]['checkout']);
            $checkout =  date_format($out,"m/d/Y"); 
            if($outout<=$checkout)
            {
                $outout = $checkout;
            }
            else{
                $outout = $outout;
            }
        }
        // $outout is the biggest date
            if($outout<$date)
            {
                $sql = "SELECT * from host where Host_ID='$hostid'";
                $result = mysqli_query($connect,$sql);
                $row = mysqli_fetch_array($result);
                $email = $row['Host_email'];
                $name = $row['Host_name'];
                $fromEmail = "yoyhomestay@gmail.com";
                $toEmail = $email;
                $subjectName = "Deletion of your account";
                $message = "You have not been active for 1 year and for that reason we had deleted your account.";
                $to = $toEmail;
                $subject = $subjectName;
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;" . "\r\n";
                $headers .= "From: ".$fromEmail."\r\n".
                            "Reply-To: ".$fromEmail."\r\n" . 
                            'X-Mailer: PHP/' . phpversion();
                $body = '<html>
                        <body>
                        <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$message.'</span>
                            <div class="container">
                                Dear '.$name.' ,<br>
                                '.$message.'<br>
                                Regards<br/>
                            '.$fromEmail.'
                            </div>
                        </body>
                        </html>';  
                if(mail($to, $subject, $body, $headers)){
                $sql ="DELETE from host WHERE Host_ID='$hostid'";
                    if(!mysqli_query($connect, $sql)) {
                        die('Error: ' . mysqli_error($connect));
                    }
                    else {   
                    ?>
                    <script> 
                        alert("Host is already deleted!") ; 
                        window.location.href = "host.php";
                    </script>
                    <?php 
                    }  
                }   
            }
            else{
                ?>
                <script> 
                    alert("The host's account cannot be deleted if the host's homestay is still booked or stayed by the guest") ;
                    window.location.href = "host.php"; w
                </script>
                <?php
            }       
        }   
}
?>