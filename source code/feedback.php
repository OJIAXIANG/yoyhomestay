<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/946f7b56f3.js" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="feedback.css">
</head>  
<?php
include("dataconnection.php");
include("header.php");
if(isset($_GET['home']))
{
    $home = $_GET['home'];
} 
else
{
    $home="";
}
$sql = "SELECT * from approved_homestay where homestay_name = '$home'";
$result = mysqli_query($connect,$sql);
$r = mysqli_fetch_assoc($result);
?>
 <div class="container border shadow mt-5 w-25 d-flex justify-content-center">
    <h2><i class="fa fa-home w3-text-teal w3-xxlarge"></i>Feedback & Rating</h2>
</div>
    <div class="container  mt-5  d-flex justify-content-center w-50 shadow-lg rounded">
        <div class="container">
            <div class="row">  
                <div class="col">        
                    <a style="font-size:25px">Homestay Name :    </a>           
                        <?php
                        echo " <a style='font-size:25px;'> ".$home."</a>"
                        ?>
                </div>
                <div class="col"> 
                        <?php
                        $dir  ='imagepreview1/';
                        echo "<img src='$dir".$r['image']."' style='width:150px; height:150px;'>";?>
                </div>
                <div class="col">         
                        <?php  
                        echo "<img src='$dir".$r['image2']."' style='width:150px; height:150px;'><br>";
                        ?>
                </div>  
            </div>        
           <div class="row">  
            <form action="#" method="POST">
             <a style="font-size:25px">Your Feedback :  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </a>           
                <textarea name="feedback" rows="4" cols="50">Your feedback.......</textarea>
           </div>  
           
                <button name="submit" class='btn btn-outline-info btn-lg'>SUBMIT</button>
            </form>
        </div>
    </div>
<?php
if(isset($_POST['submit'])){
    $sql = "SELECT * from admins where admin_id =1";
    $result = $connect->query($sql);
    if(mysqli_num_rows($result)>0)
    {
      $row = mysqli_fetch_assoc($result);
      $adminemail = $row['admin_email'];
	$fromEmail = "yoyhomestay@gmail.com";
    $toEmail = $adminemail;
    $subjectName = "Feedback";
    $message = $_POST['feedback'];
    $to = $toEmail;
    $subject = $subjectName;
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;" . "\r\n";
    $headers .= "From: ".$fromEmail."\r\n".
                "Reply-To: ".$fromEmail."\r\n" . 
                'X-Mailer: PHP/' . phpversion();
    $body = '<html>
            <body>
            <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'.$message.'</span>
                <div class="container">
                    '.$message.'<br><br>
                    Regards<br/>
                '.$fromEmail.'
                </div>
            </body>
            </html>';  
    if(mail($to, $subject, $body, $headers)){
    ?>
    <script>
        alert("Your Feedback is already sent successfully!!");
        window.location.href = "paytransc.php";
    </script>
    <?php
    }
}
}
?>