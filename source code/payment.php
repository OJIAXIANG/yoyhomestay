<DOCTYPE html>
<head>
<link rel="stylesheet" type="text/css" href="payment.css">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href='https://use.fontawesome.com/releases/v5.8.1/css/all.css'>
</head>

<!------ Include the above in your HEAD tag ---------->
<body>
<?php include("header.php"); 
include("dataconnection.php");
if(isset($_GET['bookid']))
{
  $active = $_GET['bookid'];
}
else{
  $active = "";
}
  $sql = "SELECT * from booking where active='$active'";
  if(!mysqli_query($connect, $sql)) {
    die('Error: ' . mysqli_error($connect));
}
else { 
  $res = mysqli_query($connect, $sql);
  $a = mysqli_fetch_assoc($res);
  date_default_timezone_set("Asia/Kuala_Lumpur");
  $date = date("Y-m-d");
  $pernightprice = $a['book_price'];
  $days = $a['num_days'];
  $total = $pernightprice * $days;
  $id = $a['Guest_ID'];
  $book = $a['book_id'];
  $homeid = $a['approved_id'];
  $host = $a['Host_ID'];
?>
<div class="row-25">
  <div class="col-75">
    <div class="container">
      <form action="#" method="POST">     
        <div class="row ">        
          <div class="col-50 ">
            <h3>Payment</h3>
            <img src="https://1.bp.blogspot.com/-HVoUAVuiKY0/X2toa0D8pgI/AAAAAAAAFpI/FAjcZG3foq4kSw3oX0ycqiR2BFxOGJ3jACLcBGAsYHQ/s128/master.png" style="width:10%;">
            <img src="https://1.bp.blogspot.com/-4F6wVOfxC1M/X2tprlUuV4I/AAAAAAAAFpU/l3vcv_DJ6LIBZ6QDrCKPrmc8QcfrGFqfQCLcBGAsYHQ/s0/new_visa_big.gif" style="width:10%;">
            <img src="https://1.bp.blogspot.com/-tIFA-BTbDv4/X2tsIgIZ9DI/AAAAAAAAFpg/CIJrGeP_8T0Q4VTE27rUrqDkUsE8x0XNACLcBGAsYHQ/s0/paypal-logo-transparent.png" style="width:10%;">
            <!-- pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"  -->
            <label for="cname">Name on Card</label>
            <input type="text" id="cname" name="cardname" placeholder="eg. MAYBANK" required>
            <label for="ccnum">Credit card number</label>
            <input type="text" id="ccnum" name="cardnumber" pattern="[0-9]{16}" title="16 ACC NUM" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="1111222233334444" required>
            <label for="expmonth">Exp Month</label>
            <input type="number" min="1" max="12" style="width:70%; padding:2%;"id="expmonth" name="expmonth" pattern="[0-9]{2}" title="2 NUM" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="September(09)" required>
            <label for="expyear">Exp Year</label><br>
            <input type="number" min="21" max="99"style="width:70%;padding:2%;"id="expyear" name="expyear" pattern="[0-9]{2}" title="2 NUM" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="2018(18)" required>
            <label for="cvv">CVV</label><br>
            <input type="number" min="100" max="999" style="width:70%;padding:2%;"id="cvv" name="cvv" pattern="[0-9]{3}" title="3 NUM" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="352" required><br>
            <input type="submit" name="submit" value="Payment" class="btn">
          </div>        
        </div>
         
      </form>
    </div>
  </div>
</div>
</body>
<?php
if(isset($_POST['submit']))
{
  $sql = "INSERT into pay(totalprice,paydate,Guest_ID,active,book_id,approved_id,Host_ID) 
  value ('$total','$date','$id',1,'$book','$homeid','$host')";
  if(!mysqli_query($connect, $sql)) {
    die('Error: ' . mysqli_error($connect));
}

else { 
?>
  <script> 
      alert("You may receive your receipt on next page.") ;
      window.location.href = "receipt.php";
  </script>
<?php
}
}
}



?>