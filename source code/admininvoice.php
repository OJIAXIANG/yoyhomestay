<?php
include ("header.php");
?>
<!DOCTYPE html>
<head>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<!------ Include the above in your HEAD tag ---------->
</head>
<style>
.container
{
	padding-top:40px;
}
</style>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Invoice</h2><h3 class="pull-right">Order # 12345</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Billed To:</strong><br>
    					John Smith<br>
    					johnsmith@gmail.com<br>
    					016-3456789<br>
    					
    				</address>
    			</div>
    			
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Payment Method:</strong><br>
    					Credit Card<br>
    					johnsmith@gmail.com
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Order Date:</strong><br>
    					September 6, 2020<br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Payment</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Product</strong></td>
        							<td class="text-center"><strong>#</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-right"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<tr>
    								<td>Number of Days Stayed</td>
    								<td class="text-center">3</td>
    								<td class="text-center">RM80</td>
    								<td class="text-right">RM240</td>
    							</tr>
								<tr>
    								<td>Number of Guests</td>
    								<td class="text-center">5</td>
    								<td class="text-center"></td>
    								<td class="text-right"></td>
    							</tr>
								<tr>
    								<td>Check-in date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1/9/2020]</td>
    								<td class="text-center"></td>
    								<td class="text-center"></td>
    								<td class="text-right"></td>
    							</tr>
								<tr>
    								<td>Check-out date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[4/9/2020]</td>
    								<td class="text-center"></td>
    								<td class="text-center"></td>
    								<td class="text-right"></td>
    							</tr>
								
								<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">RM240</td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
</body>
</html>