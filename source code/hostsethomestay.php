<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="image.css">
<link rel="stylesheet" type="text/css" href="login.css">
<link rel="stylesheet" type="text/css" href="scrollbar.css">

<link rel="stylesheet" type="text/css" href="hostsethomestay.css">
<title>Image Upload</title>
<style>
</style>

</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <meta name="description" content="">

    <title>User profile form requirement</title>


    <!-- Bootstrap Core CSS -->
<!--     <link href="css/bootstrap.min.css" rel="stylesheet"> -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">

<?php include("header.php");?>
<div class="header">
<h2>&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
 &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
 &nbsp;  Set your homestay</h2>
 <?php
 ?>
<body>
	<div class="tab">
			<button class="tablinks" onclick="openCity(event, 'Homestay')" id="defaultOpen">Homestay</button>
			<button class="tablinks" onclick="openCity(event, 'Description')">Description</button>
			<button class="tablinks" onclick="openCity(event, 'Location')">Location</button>
			<button class="tablinks" onclick="openCity(event, 'Room')">Room</button>
	</div>
	
<form method="POST" action="#" enctype="multipart/form-data">

<div id="Homestay" class="tabcontent">
  <h3>&nbsp;&nbsp;Homestay</h3>
  <div class="col-sm-7">
	
  <div class="align_centers">
	  <br><h4>Listing Title<h4>
	   <input type="text" id="listing_title" name="homestay_name" maxlength="25" size="30px"placeholder="Insert your homestay name" required >
	
</div>

	</div> 
	<div class="col-sm-6">
		<div class="tabcontent_homestay">
			<div class="form-input">
				<label for="file-ip-1">Upload Image</label>
				<input type="file" onchange="showPreview(event)" id="file-ip-1" name="image" accept="image/x-png,image/gif,image/jpeg,image/jpg" required>
				
				<div class="preview">
					<img style="height: 265px; width:255px;" id="file-ip-1-preview">
				</div>
			</div>
		</div>
		
		<div class="align_center">
			<div>
			<textarea 
				id="text" 
				cols="32" 
				rows="3" 
				name="image_text" 
				maxlength="30"
				placeholder="Say something about this image..."></textarea>
			</div>
		</div>
	</div>
	
	<div class="col-sm-6">
	 <div class="tabcontent_homestay_2">
			<div class="form-input">
				<label for="file-ip-2">Upload Image</label>
				 
				<input type="file" onchange="ShowPreview_2(event)" id="file-ip-2" name="image2" accept="image/x-png,image/gif,image/jpeg,image/jpg" required>
				
				<div class="preview">
				
					<img style="height: 265px; width:255px;"id="file-ip-1-preview-2">
			
				</div>
			</div>
	</div>
	
		
		
		<div class="align_center">
		<div>
		  <textarea 
			id="text" 
			cols="32" 
			rows="3" 
			name="image_text2" 
			maxlength="30"
			placeholder="Say something about this image..."></textarea>
		</div>
		</div>
	</div>
		
</div>
<script>
function showPreview(event)
{
	if(event.target.files.length>0)
	{
		var src = URL.createObjectURL(event.target.files[0]);
		var preview = document.getElementById("file-ip-1-preview");
		preview.src = src;
		preview.style.display = "block";
	}
	
}

</script>

<script>
function ShowPreview_2(event)
{
	if(event.target.files.length>0)
	{
		var src = URL.createObjectURL(event.target.files[0]);
		var preview = document.getElementById("file-ip-1-preview-2");
		preview.src = src;
		preview.style.display = "block";
	}
	
}

</script>

<div id="Description" class="tabcontent">

 
	<h3>&nbsp;&nbsp;About</h3> 
	 <div class="col-sm-7">
	
	 <textarea 
			id="text" 
			cols="80" 
			rows="6" 
			name="about" 
			maxlength="150"
			placeholder="Describe your homestay"  required></textarea>
    </div>

<div class="col-sm-6">
	  
	  <br><h4>House Type<h4>
	   <select id="house_type" name="housetype" required>
			<option value="ps">Please select</option>
			<option value="Terrance">Terrace</option>
			<option value="Semi-detached">Semi-detached</option>
			<option value="Detached">Detached</option>
			<option value="Bungalow">Bungalow</option>
			<option value="Duplex">Duplex</option>
			<option value="Apartment">Apartment</option>
			<option value="Villa">Villa</option>
			<option value="Cottage">Cottage</option>
			<option value="Cabin">Cabin</option>
			<option value="House">House</option>
		</select>
  </div> 
	
	
  
</div>



<div id="Location" class="tabcontent">
  <h3>&nbsp;&nbsp;Location of your homestay</h3>
 
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAavT_xkwxtR3MPn2lvsJ5TOtcEYYDbK2g&libraries=places">
 </script>
 <script type="text/javascript">


     function initialize() {
       autocomplete = new google.maps.places.Autocomplete(
         /** @type {HTMLInputElement} */
         (document.getElementById('InputAddressSearch')),
         { types: ['geocode'] }
       );
       google.maps.event.addListener(autocomplete, 'place_changed', fillInAddress);
     }

/*
function initialize() {
  var input = document.getElementById('InputAddressSearch');
  new google.maps.places.Autocomplete(input);
}
*/
google.maps.event.addDomListener(window, 'load', initialize);

</script>

<div class="col-md-12 mx-auto form-label">
		<label for="InputAddressSearch" ><br><h4>Address Line</h4></label>
		<i class="" data-toggle="" data-placement="top" title="Type your address here and google will look for it"></i>
		<input type="text"
				class="form-control form-control-lg form-field"
				style="text-align: left;"
				id="InputAddressSearch"
				name="address_line" 
				aria-describedby="InputAddressSearch"
				placeholder="Type your address here" required>
	</div>
	
 <div class="col-sm-7">
 <br><h4>Complete Address<h4>
	 <textarea 
			id="text" 
			cols="80" 
			rows="6" 
			name="fulladdressline" 
			maxlength="300"
			placeholder="Type your homestay complete address which include house number"  maxlength="300" required></textarea>
 </div>

	
  
</div>


<div id="Room" class="tabcontent">
<h3>&nbsp;&nbsp;Set your room for your homestay</h3>
<br>
<div class="col-sm-6">
	<h4>Number of room<h4>

	   <select id="guest" name="no_of_room" required>
		
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
			<option value="7">7</option>
			<option value="8">8</option>
			<option value="9">9</option>
			<option value="10">10</option>
		</select>
		<h6>***Only include the number of bedrooms and 1 storage room !!!</h6>
	</div>
	
  <div class="col-sm-7">
	<br><br><h4>Homestay price per night: </h4>
	RM <input type="number" id="quantity" name="price" min="0" max="4800" size="50" required><br>
	<h6>***The price must be RM300 or above.</h6>
	
  </div>
  
  	<div class="col-sm-7">
  		<br><br><button type="submit"  name="upload" style="height:50px;">UPLOAD</button>

  	</div>
</div>	
</form>
</div>


<script>

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

</script>

<?php

	
  // Create database connection
  include("dataconnection.php");

  // Initialize message variable
  $msg = "";

  // If upload button is clicked ...
  if (isset($_POST['upload'])) 
  {
	$homestay_name=$_POST['homestay_name'];
	$about=$_POST['about'];
	$housetype=$_POST['housetype'];
	$address_line=$_POST['address_line'];
	$fulladdressline=$_POST['fulladdressline'];
	$no_of_room=$_POST['no_of_room'];
	$price=$_POST['price'];
	
	// Get image name
	 $img = $_FILES['image']['name'];
	 $img2 = $_FILES['image2']['name'];
	 
	 //add hostid
	 $image = $_SESSION['hostid'].'_'.$img;
	 $image2 = $_SESSION['hostid'].'_'.$img2;
	 	 
  	// Get text
  	$image_text = mysqli_real_escape_string($connect, $_POST['image_text']);
	$image_text2 = mysqli_real_escape_string($connect, $_POST['image_text2']);

	$sql = "INSERT INTO upload_homestay (image,image2,image_text,image_text2,homestay_name,about_homestay,house_type,address_line,full_address,number_of_room,price_per_night,Host_ID) VALUES ('$image','$image2', '$image_text', '$image_text2','$homestay_name','$about','$housetype','$address_line','$fulladdressline','$no_of_room','$price','".$_SESSION['hostid']."')";

	// image file directory
  	$target = "imagepreview1/".basename($image);
	$target2 = "imagepreview1/".basename($image2);
	// execute query
  	mysqli_query($connect, $sql);

  	if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
  		$msg = "Image uploaded successfully";
  	}else{
  		$msg = "Failed to upload image";
	  }
	if (move_uploaded_file($_FILES['image2']['tmp_name'], $target2)) {
		$msg = "Image uploaded successfully";
	}else{
		$msg = "Failed to upload image";
	}
?>
       <script type="text/javascript">;
		alert("Your homestay detail has been uploaded.Please wait for the approval. Thank you.<?php echo $image,$image2 ?>");
		window.location.href = "hosthomepage.php";
       </script>;

<?php
 }
  $result = mysqli_query($connect, "SELECT * FROM upload_homestay");
?>

</body>
</html>