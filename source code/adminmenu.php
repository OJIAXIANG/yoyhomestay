
<!DOCTYPE html>
<head>

</head>
<style>
body
{
	background-size:100%;
	background-repeat:no-repeat; 
	background-color:#4A6FA5;
	margin-bottom:50px;
}
.tab {
    margin: 0 10%;
}
button.tablinks:first-child {
    margin-right: 10px;
}
button.tablinks.active {
    opacity: 1;
    background: #fefefe;

h3{
    padding-left: 10px;
}

.containerjx {
    width: 80%;
    padding: 10px 2%;
    margin: 0% 10% 1%;
    border-radius: 5px;
    border-top-left-radius: 0px;
    box-shadow: 0px 0px 2px #555;
    background: #fefefe;
    min-height: 45em;
}
table{
	width: 100%;
    /* box-shadow: 0px 0px 4px 1px black; */
    border: 1px solid #d1d1d1;
	border-collapse: collapse;
}
.tablinks {
    margin-right: 7px;
    opacity: 0.65;
    background: #f1f1f1;
    border: none;
    border-color: #ffff;
    border-radius: 3px 3px 0 0;
    background-color: #ffffff;
    border-top: 5px solid #9d2235;
    box-shadow: 0 0 2px #555;
    -webkit-box-shadow: 0px -1px 2px #555;
    font-weight: 600;
    padding: 15px 25px !important;
}

</style>
</html>