<?php include("dataconnection.php"); ?>
<?php include("header.php"); ?>
<html>
<head><title>Admin List</title>
<link href="admincss.css" type="text/css" rel="stylesheet" />




</head>
<style>
table
{
	margin-left:50px;
	margin-right:50px;
	
	padding:10px;
}
</style>
<body>

<div id="wrapper">
	<div id="editadminright">

		<h1>List of Admins</h1>

		<table border="5">
			<tr>
				<th>Staff ID</th>
				<th>Admin Name</th>
				<th>Admin Email</th>
				<th>Admin Password</th>
				<th>Admin Contact</th>
				<th colspan="2">Action</th>
			</tr>
			<!--can copy paste the code from moview_list(view_details).php and add on edit hyperlink-->
			<?php
			
			$result = mysqli_query($connect, "SELECT * from admins");	
			$count = mysqli_num_rows($result);//used to count number of rows
			
			while($row = mysqli_fetch_assoc($result))
			{
			
			?>			

			<tr>
				<td><?php echo $row["staff_id"]; ?></td>
				<td><?php echo $row["admin_name"]; ?></td>
				<td><?php echo $row["admin_email"]; ?></td>
				<td><?php echo $row["admin_password"]; ?></td>
				<td><a href="admin(detail).php?id=<?php echo $row['admin_id']; ?>">More Details</a></td>
				<td><a href="admin_edit.php?id=<?php echo $row['admin_id']; ?>">Edit</a> </td>
				
			</tr>
			<?php
			
			}
			
			?>
			
		</table>


		<p> Number of records : <?php echo $count; ?></p>

	</div>
	
</div>


</body>
</html>

