-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2020 at 08:24 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `homestay`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(3) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_contact` varchar(255) NOT NULL,
  `staff_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_contact`, `staff_id`) VALUES
(1, 'YAP YI XIANG', 'yaptungquan@gmail.com', '123456', '0123456789', 'YOY01'),
(2, 'YEOW JIA QUAN', 'xiang@hotmail.com', 'abcdef', '0123456789', 'YOY02'),
(3, 'VSOY KEONG ', 'keong@gmail.com', 'hahahaha', '012345679', 'YOY03');

-- --------------------------------------------------------

--
-- Table structure for table `approved_homestay`
--

CREATE TABLE `approved_homestay` (
  `approved_id` int(255) NOT NULL,
  `homestay_id` int(255) NOT NULL,
  `homestay_name` text NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image_text` text CHARACTER SET latin1 NOT NULL,
  `image_text2` text CHARACTER SET latin1 NOT NULL,
  `about_homestay` text CHARACTER SET latin1 NOT NULL,
  `house_type` text NOT NULL,
  `address_line` text NOT NULL,
  `full_address` text NOT NULL,
  `number_of_room` int(15) NOT NULL,
  `price_per_night` int(4) NOT NULL,
  `Host_ID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `approved_homestay`
--

INSERT INTO `approved_homestay` (`approved_id`, `homestay_id`, `homestay_name`, `image`, `image2`, `image_text`, `image_text2`, `about_homestay`, `house_type`, `address_line`, `full_address`, `number_of_room`, `price_per_night`, `Host_ID`) VALUES
(28, 1, 'Nice Home', '1_nice2.jpeg', '1_nice.jpeg', 'Infront Homestay', 'Inside Homestay', 'Nice Homestay and Nice place', 'Bungalow', 'Alor Gajah, é©¬å…­ç”²é©¬æ¥è¥¿äºš', 'NO 14,JALAN BUKIT BARU 12 ,TAMAN BUKIT BARU,ALOR GAJAH', 3, 400, 1),
(29, 2, 'Bryan haha', '1_nice2.jpeg', '1_nice.jpeg', 'hehe', 'hehe', 'hello welcome', 'Bungalow', 'Melaka Raya, é©¬å…­ç”²é©¬æ¥è¥¿äºš', 'no 16 jalan melaka raya', 3, 200, 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `book_id` int(15) NOT NULL,
  `homestay_name` text NOT NULL,
  `book_price` int(15) NOT NULL,
  `checkin` text NOT NULL,
  `checkout` text NOT NULL,
  `num_guest` int(255) NOT NULL,
  `num_days` int(255) NOT NULL,
  `approved_id` int(255) NOT NULL,
  `Guest_ID` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `Host_ID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`book_id`, `homestay_name`, `book_price`, `checkin`, `checkout`, `num_guest`, `num_days`, `approved_id`, `Guest_ID`, `active`, `Host_ID`) VALUES
(48, 'Bryan haha', 200, '10/09/2020', '10/13/2020', 13, 4, 29, 1, 0, 1),
(49, 'Nice Home', 400, '10/08/2020', '10/17/2020', 12, 9, 28, 1, 0, 1),
(50, 'Nice Home', 400, '10/22/2020', '10/24/2020', 3, 2, 28, 1, 0, 1),
(51, 'Bryan haha', 200, '10/15/2020', '10/17/2020', 4, 2, 29, 1, 0, 1),
(52, 'Nice Home', 400, '10/27/2020', '10/30/2020', 12, 3, 28, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `Guest_ID` int(11) NOT NULL,
  `Guest_Name` varchar(99) NOT NULL,
  `Guest_contact` varchar(15) NOT NULL,
  `Guest_email` varchar(50) NOT NULL,
  `Guest_password` varchar(255) NOT NULL,
  `profileimage` varchar(255) NOT NULL,
  `guest_info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`Guest_ID`, `Guest_Name`, `Guest_contact`, `Guest_email`, `Guest_password`, `profileimage`, `guest_info`) VALUES
(1, 'Jia Xiang', '0146125242', 'xiang_0408@hotmail.com', '123AAbb*', '11_WeChat Image_20181018101139.jpg', 'limbeh shi wang lei de baba apajaychou'),
(2, 'jason', '0123456789', 'xiang@hotmail.com', 'das', 'bean.jpg', 'hello martin is a good boy'),
(3, 'shenghao', '0123456789', 'shenghao013@gmail.com', '1212AAbb**', 'kobe_bryant_basketball_nba_los_angeles_lakers_98675_1366x768.jpg', ''),
(19, 'Bryan', '0123456789', 'bryanding2000@gmail.com', '123AAbb**', '19_WeChat Image_20181018101139.jpg', 'testing one two three');

-- --------------------------------------------------------

--
-- Table structure for table `host`
--

CREATE TABLE `host` (
  `Host_ID` int(5) NOT NULL,
  `Host_N` varchar(99) NOT NULL,
  `Host_email` varchar(50) NOT NULL,
  `Host_password` varchar(10) NOT NULL,
  `Host_Contact` varchar(15) NOT NULL,
  `hostimage` varchar(255) NOT NULL,
  `host_info` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `host`
--

INSERT INTO `host` (`Host_ID`, `Host_N`, `Host_email`, `Host_password`, `Host_Contact`, `hostimage`, `host_info`) VALUES
(1, 'Yi Feng', 'yeowyifeng98@gmail.com', '123AAbb**', '0123456789', '4_yifeng.jpg', 'halo apa lu mau');

-- --------------------------------------------------------

--
-- Table structure for table `pay`
--

CREATE TABLE `pay` (
  `pay_id` int(15) NOT NULL,
  `totalprice` varchar(244) NOT NULL,
  `paydate` date NOT NULL,
  `Guest_ID` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `book_id` int(15) NOT NULL,
  `approved_id` int(255) NOT NULL,
  `Host_ID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pay`
--

INSERT INTO `pay` (`pay_id`, `totalprice`, `paydate`, `Guest_ID`, `active`, `book_id`, `approved_id`, `Host_ID`) VALUES
(6, '800', '2020-10-07', 1, 0, 48, 29, 1),
(7, '3600', '2020-10-07', 1, 0, 49, 28, 1),
(8, '800', '2020-10-07', 1, 0, 50, 28, 1),
(9, '800', '2020-10-07', 1, 0, 50, 28, 1),
(10, '1200', '2020-10-07', 1, 0, 52, 28, 1);

-- --------------------------------------------------------

--
-- Table structure for table `super`
--

CREATE TABLE `super` (
  `super_id` int(3) NOT NULL,
  `super_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `super_email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `super_contact` varchar(255) CHARACTER SET latin1 NOT NULL,
  `super_password` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `super`
--

INSERT INTO `super` (`super_id`, `super_name`, `super_email`, `super_contact`, `super_password`) VALUES
(1, 'Martin Yap', 'yoyhomestay@gmail.com', '01136692463', 'yoyhomestay989900');

-- --------------------------------------------------------

--
-- Table structure for table `upload_homestay`
--

CREATE TABLE `upload_homestay` (
  `homestay_id` int(254) NOT NULL,
  `homestay_name` text NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image_text` text CHARACTER SET latin1 NOT NULL,
  `image_text2` text CHARACTER SET latin1 NOT NULL,
  `about_homestay` text CHARACTER SET latin1 NOT NULL,
  `house_type` text NOT NULL,
  `address_line` text NOT NULL,
  `full_address` text NOT NULL,
  `number_of_room` int(15) NOT NULL,
  `price_per_night` int(4) NOT NULL,
  `Host_ID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `approved_homestay`
--
ALTER TABLE `approved_homestay`
  ADD PRIMARY KEY (`approved_id`),
  ADD KEY `Host_ID` (`Host_ID`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `approved_id` (`approved_id`),
  ADD KEY `Guest_ID` (`Guest_ID`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`Guest_ID`);

--
-- Indexes for table `host`
--
ALTER TABLE `host`
  ADD PRIMARY KEY (`Host_ID`);

--
-- Indexes for table `pay`
--
ALTER TABLE `pay`
  ADD PRIMARY KEY (`pay_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `super`
--
ALTER TABLE `super`
  ADD PRIMARY KEY (`super_id`);

--
-- Indexes for table `upload_homestay`
--
ALTER TABLE `upload_homestay`
  ADD PRIMARY KEY (`homestay_id`),
  ADD KEY `Host_ID` (`Host_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `approved_homestay`
--
ALTER TABLE `approved_homestay`
  MODIFY `approved_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `book_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `Guest_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `host`
--
ALTER TABLE `host`
  MODIFY `Host_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pay`
--
ALTER TABLE `pay`
  MODIFY `pay_id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `super`
--
ALTER TABLE `super`
  MODIFY `super_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `upload_homestay`
--
ALTER TABLE `upload_homestay`
  MODIFY `homestay_id` int(254) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `approved_homestay`
--
ALTER TABLE `approved_homestay`
  ADD CONSTRAINT `approved_homestay_ibfk_1` FOREIGN KEY (`Host_ID`) REFERENCES `host` (`Host_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `Guest_ID` FOREIGN KEY (`Guest_ID`) REFERENCES `guest` (`Guest_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `approved_id` FOREIGN KEY (`approved_id`) REFERENCES `approved_homestay` (`approved_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pay`
--
ALTER TABLE `pay`
  ADD CONSTRAINT `pay_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `booking` (`book_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `upload_homestay`
--
ALTER TABLE `upload_homestay`
  ADD CONSTRAINT `upload_homestay_ibfk_1` FOREIGN KEY (`Host_ID`) REFERENCES `host` (`Host_ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
